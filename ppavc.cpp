/*
    ppavc - planned program for audio and video conversion
    Copyright (C) 2016 - 2022 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <iostream>
#include <algorithm>
#include <memory>
#include <vector>


#include <fmt/core.h>


#include "Containers/RIFFContainer.h" //only in early stage development!
#include "Codecs/PCMCodec.h" //only in early stage development!

#include "Core/Exceptions.h"
#include "Core/FileResource.h"
#include "Core/ConversionManager.h"


using namespace PPAVC;


void print_codec(std::shared_ptr<Codec> codec)
{
    if (codec == nullptr) {
        return;
    }

    std::cout << fmt::format(
        "Codec: {0}\n",
        codec->getName()
        );
    /*
    if (stream.type == STREAM_AUDIO) {
        std::cout << fmt::format(
            "AUDIO data:\n"
            "----\n"
            "samples/second: {0}\n"
            "resolution: {1} bit\n"
            "channels: {2}\n",
            stream.audio_samples_per_second,
            stream.audio_resolution,
            stream.audio_channels
            );
    } else if (stream.type == STREAM_VIDEO) {
        std::cout << fmt::format(
            "VIDEO data:\n"
            "----\n"
            "resolution: {0}x{1}\n"
            "pixel_format: {2}\n",
            stream.video_width,
            stream.video_height,
            stream.video_pixel_format
            );
    } else if (stream.type == STREAM_TEXT) {
        std::cout <<
            "TEXT data:\n"
            "----\n";
    } else {
        std::cout << "UNKNOWN data!\n";
    }
    */
}


void print_resource(Resource& resource)
{
    std::cout << fmt::format(
        "Resource #{0}:\n"
        "--------\n"
        "Location: {1}\n"
        "Metadata:\n"
        //place content of resource.metadata below here.
        "----\n",
        resource.getId(),
        resource.getLocation()
        );
}


int main(int argc, char** argv)
{
    std::cout << "ppavc  Copyright (C) 2016-2022  Moritz Strohm <ncc1988@posteo.de>\n";

    if(argc >= 2) {
        // Assume that a multimedia file was specified: Try to read its header.
        std::string file_name(argv[1]);
        std::shared_ptr<ConversionManager> conversion_manager = nullptr;
        try {
            conversion_manager = std::make_shared<ConversionManager>(file_name);
        } catch (Exception &e) {
            std::cerr << fmt::format("Error opening resource: {0}", e.getMessage()) << std::endl;
            return 1;
        }
        if (argc >= 3) {
            std::string output_file_name(argv[2]);
            std::shared_ptr<FileResource> output_file = nullptr;
            try {
                output_file = std::make_shared<FileResource>(output_file_name, true);
            } catch (Exception& e) {
                std::cerr << fmt::format("Error opening resource: {0}", e.getMessage()) << std::endl;
                return 1;
            }

            if (output_file == nullptr) {
                std::cerr << "Error initialising resource writing module!" << std::endl;
                return 1;
            }
            conversion_manager->addOutputResource(output_file);
        }

        std::pair<StreamID, StreamID> mapping(
            StreamID(0, 0, StreamType::AUDIO),
            StreamID(0, 0, StreamType::AUDIO)
            );

        conversion_manager->setMapping({mapping});

        try {
            conversion_manager->convert();
        } catch (Exception& e) {
            std::cerr << fmt::format("Exception caught: {0}", e.getMessage()) << std::endl;
        }
    } else {
        std::cerr << "Usage: ppavc INFILE OUTFILE" << std::endl;
    }
    return 0;
}
