/*
    Container.cpp
    This file is part of ppavc - planned program for audio and video conversion.
    Copyright (C) 2016 - 2022 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "Container.h"


using namespace PPAVC;


std::map<uint8_t, std::string> Container::getAudioStreams()
{
    return this->audio_streams;
}


std::map<uint8_t, std::string> Container::getVideoStreams()
{
    return this->video_streams;
}


std::string Container::getAudioCodec(uint8_t audio_stream_id)
{
    try {
        auto codec = this->audio_streams.at(audio_stream_id);
        return codec;
    } catch (std::out_of_range& e) {
        return "";
    }
}


void Container::setAudioCodec(uint8_t audio_stream_id, const std::string& codec)
{
    if (codec == "") {
        return;
    }
    if (this->audio_streams.size() > audio_stream_id) {
        //Replace an existing codec.
        this->audio_streams[audio_stream_id] = codec;
    } else if (this->audio_streams.size() == audio_stream_id) {
        //Append a new codec.
        this->audio_streams.insert({audio_stream_id, codec});
    }
}


AudioStreamConfig Container::getAudioStreamConfig(uint8_t audio_stream_id)
{
    try {
        auto config = this->audio_stream_configs.at(audio_stream_id);
        return config;
    } catch (std::out_of_range& e) {
        return AudioStreamConfig();
    }
}


void Container::setAudioStreamConfig(uint8_t audio_stream_id, const AudioStreamConfig& config)
{
    this->audio_stream_configs[audio_stream_id] = config;
}


std::string Container::getVideoCodec(uint8_t video_stream_id)
{
    try {
        auto codec = this->video_streams.at(video_stream_id);
        return codec;
    } catch (std::out_of_range& e) {
        return "";
    }
}


void Container::setVideoCodec(uint8_t video_stream_id, const std::string& codec)
{
    if (codec == "") {
        return;
    }
    if (this->video_streams.size() > video_stream_id) {
        //Replace an existing codec.
        this->video_streams[video_stream_id] = codec;
    } else if (this->video_streams.size() == video_stream_id) {
        //Append a new codec.
        this->video_streams.insert({video_stream_id, codec});
    }
}


VideoStreamConfig Container::getVideoStreamConfig(uint8_t video_stream_id)
{
    try {
        auto config = this->video_stream_configs.at(video_stream_id);
        return config;
    } catch (std::out_of_range& e) {
        return VideoStreamConfig();
    }
}
