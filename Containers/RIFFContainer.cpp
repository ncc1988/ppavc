/*
    RIFFContainer.cpp
    This file is part of ppavc - planned program for audio and video conversion.
    Copyright (C) 2016 - 2022 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "RIFFContainer.h"


using namespace PPAVC;


//protected


bool RIFFContainer::readChunk(size_t position = 0)
{
    // A RIFF chunk has the following format:
    // - 4 bytes: chunk identifier (e.g. "fmt ")
    // - 4 bytes: chunk size (without identifier and size)
    // - variable amount of bytes: chunk specific data
    //
    // Return value:
    // true, if a chunk was read
    // false, if no chunk was read

    if (position == 0) {
        //Position = 0 => use internal next chunk position, if set.
        if (this->next_chunk_position != 0) {
            position = next_chunk_position;
        }
        // If next_chunk_position is 0, then we may try to read from position 0.
    }

    if (this->resource.expired()) {
        return false;
    }

    auto resource_ptr = this->resource.lock();
    if (resource_ptr == nullptr) {
        return false;
    }
    RIFFChunk chunk;
    resource_ptr->readObject(reinterpret_cast<uint8_t*>(&chunk), sizeof(RIFFChunk), position);

    chunk.id = htole32(chunk.id);

    //All required data for the chunk_list vector are present: add one chunk to the list:
    this->chunk_list.insert(std::pair<size_t, RIFFChunk>(this->next_chunk_position, chunk));
    this->next_chunk_position = position + chunk.size + sizeof(RIFFChunk);

    //Check if the first 32 bit spell "fmt " in little endian (" tmf"):
    if (chunk.id == 0x20746d66) {
        if (this->riff_type == RIFFType::WAVE) {
            if ((chunk.size == 16) || (chunk.size == 18)) {
                //The WAVE header has the correct size (16 bytes or 18 bytes).
                //Now we're reading the WAVE header:
                RIFFWaveFormatData wave_header;
                resource_ptr->readObject(reinterpret_cast<uint8_t*>(&wave_header), sizeof(RIFFWaveFormatData));

                //This may be the header version, should be 1:
                if (le16toh(wave_header.format) == 0x01) {
                    //Store the stream configuration:
                    PPAVC::AudioStreamConfig config;
                    config.channels = le16toh(wave_header.audio_channels);
                    config.bits_per_sample = le16toh(wave_header.bits_per_sample);
                    config.sample_rate = le32toh(wave_header.sample_rate);
                    this->audio_streams.insert({0, "PCM"});
                    this->audio_stream_configs.insert({0, config});
                } else {
                    //wrong version: raise exception
                    throw Exception("RIFF WAVE header version number is wrong!");
                }
            } else {
                // The WAVE header has the wrong size: raise exception.
                throw Exception(fmt::format("RIFF WAVE header size is wrong! ({0} instead of 16)", chunk.size));
            }
        } else if(this->riff_type == RIFFType::AVI) {
            //Do things that are done with RIFF AVI files.
            return false;
        } else {
            //Unknown RIFF type: return false!
            return false;
        }
    } else if (chunk.id == 0x5453494C) {
        //"LIST" in little endian ("TSIL")

        //NOT IMPLEMENTED YET! But skip it:
        position += 4;
    } else if (chunk.id == 0x61746164) {
        //"data" in little endian ("atad")
        if (this->riff_type == RIFFType::WAVE) {
            //Only one stream should exist:
            if (this->audio_streams.size() == 1) {
                //Set the stream start position after the end of this chunk:
                this->audio_stream_positions[0] = position + 8;
            }
        }
    } else {
        //RIFF type is unknown.
        return false;
    }

    return true;
}



void RIFFContainer::readChunkHeaders()
{
    //Read all the chunks headers:
    while(this->readChunk() == true);
}


std::pair<size_t, RIFFChunk> RIFFContainer::findChunk(uint32_t id = 0)
{
    for (auto entry: this->chunk_list) {
        if (entry.second.id == id) {
            return entry;
        }
    }
    //we haven't found the chunk: Return an empty chunk.
    return {-1, RIFFChunk()};
}


//public


RIFFContainer::RIFFContainer(std::shared_ptr<Resource> resource)
    : Container(resource)
{
    this->riff_type = RIFFType::UNSPECIFIED;
    this->next_chunk_position = 0;
}


void RIFFContainer::readHeader()
{
    if (this->resource.expired()) {
        return;
    }

    auto resource_ptr = this->resource.lock();
    if (resource_ptr == nullptr) {
        return;
    }
    //Get RIFF header (12 bytes):
    // The RIFF header has the following structure:
    // - 4 bytes: "RIFF"
    // - 4 bytes: first chunk start position, relative to the end of the data source
    // - 4 bytes: type: "WAVE" or "AVI "
    RIFFParentChunk riff_chunk;
    resource_ptr->readObject(reinterpret_cast<uint8_t*>(&riff_chunk), sizeof(RIFFParentChunk));

    //We can convert two parts of the RIFF header directly to little endian
    //to make string comparison easy on big and little endian machines.
    riff_chunk.id  = htole32(riff_chunk.id);
    riff_chunk.type = htole32(riff_chunk.type);

    //Check for the ID of the RIFF chunk. They must contain "RIFF",
    //stored in little endian ("FFIR")
    if (riff_chunk.id == 0x46464952) {
        //The next chunks start position must be directly after
        //the RIFF chunk:
        this->next_chunk_position = 12;
        if (riff_chunk.type == 0x45564157) {
            //"WAVE" in little endian ("EVAW")
            // WAVE content:
            // one audio stream, no video stream, no text stream

            this->riff_type = RIFFType::WAVE;
        } else if(riff_chunk.type == 0x20495641) {
            //"AVI " in little endian (" IVA")

            //AVI content: we must read more chunks to determine
            // the number of audio, video and text streams.
            this->riff_type = RIFFType::AVI;
        } else {
            //unknown content: raise exception
            throw Exception("Unknown content in RIFF header!");
        }
        //Read the next chunk header:
        this->readChunkHeaders();
    } else {
        //raise exception: no RIFF header found!
        throw Exception("No RIFF header found!");
    }
}


void RIFFContainer::writeHeader()
{
    //In this method we're building a RIFF header out of the Resource data.

    RIFFParentChunk riff_chunk;
    riff_chunk.id = 0x46464952; //"RIFF" in little endian

    /*
    if((this->resource->streams.size() == 1) && (this->resource->streams.begin()->type == STREAM_AUDIO)) {
        //It is a RIFF WAVE container:
        riff_chunk.type = 0x45564157; //"WAVE" in little endian
    }
    */
    //STUB, until the streams are enabled again:
    riff_chunk.type = 0x45564157; //"WAVE" in little endian

    //wave chunk header:
    RIFFChunk wave_chunk;
    wave_chunk.id = 0x20746d66; //"fmt " in little endian
    wave_chunk.size = 16; //the size in bytes

    if (this->audio_streams.empty()) {
        throw Exception("No audio streams in container!");
    }
    AudioStreamConfig config;
    try {
        config = this->audio_stream_configs.at(0);
    } catch (std::out_of_range& e) {
        throw Exception("Stream without codec configuration!");
    }
    //Initialise the audio stream data counter for the stream:
    this->audio_stream_written_data[0] = 0;

    RIFFWaveFormatData format_data(1, config.channels, config.sample_rate, config.bits_per_sample);

    //At this point we know the size of the RIFF chunk:
    riff_chunk.size = sizeof(RIFFParentChunk) + sizeof(RIFFChunk) + sizeof(RIFFWaveFormatData);

    //Now we can add a data chunk:
    RIFFChunk data_chunk;
    data_chunk.id = 0x61746164; //"data" in little endian ("atad")
    data_chunk.size = this->audio_stream_written_data[0];

    if (!this->resource.expired()) {
        auto resource_ptr = this->resource.lock();
        if (resource_ptr != nullptr) {
            //We can write the header, but we must keep track of the position
            //so that we can jump back to the data chunk later to correct
            //its size.
            size_t position = 0;
            resource_ptr->writeObject(reinterpret_cast<uint8_t*>(&riff_chunk), sizeof(RIFFParentChunk), 0);
            position += sizeof(RIFFParentChunk);
            resource_ptr->writeObject(reinterpret_cast<uint8_t*>(&wave_chunk), sizeof(RIFFChunk), position);
            this->chunk_list[position] = wave_chunk;
            position += sizeof(RIFFChunk);
            resource_ptr->writeObject(reinterpret_cast<uint8_t*>(&format_data), sizeof(RIFFWaveFormatData), position);
            position += sizeof(RIFFWaveFormatData);
            resource_ptr->writeObject(reinterpret_cast<uint8_t*>(&data_chunk), sizeof(RIFFChunk), position);
            this->chunk_list[position] = data_chunk;
            this->header_written = true;
        }
    }
}


void RIFFContainer::updateHeader()
{
    if (this->resource.expired()) {
        //We cannot update the header.
        return;
    }
    auto resource_ptr = this->resource.lock();
    if (resource_ptr == nullptr) {
        //We cannot update the header.
        return;
    }
    try {
        auto data_size = this->audio_stream_written_data.at(0);
        uint32_t full_size = 0;
        for (auto chunk_item: this->chunk_list) {
            auto position = chunk_item.first;
            auto chunk = chunk_item.second;
            if (chunk.id == 0x61746164) {
                //We found the right chunk to update.
                chunk.size = data_size;
                resource_ptr->writeObject(reinterpret_cast<uint8_t*>(&chunk), sizeof(RIFFChunk), position);
            }
            full_size += chunk.size;
        }
        //Update the RIFF chunk size value:
        resource_ptr->writeObject(reinterpret_cast<uint8_t*>(&full_size), sizeof(uint32_t), 4);
    } catch (std::out_of_range& e) {
        //Map item not initialised. We cannot update the header.
    }
}


std::vector<uint8_t> RIFFContainer::readAudioData(uint8_t audio_stream_id, size_t bytes)
{
    if (this->resource.expired()) {
        return {};
    }
    auto resource_ptr = this->resource.lock();
    if (resource_ptr == nullptr) {
        return {};
    }
    auto stream_position_it = this->audio_stream_positions.find(audio_stream_id);
    if (stream_position_it == this->audio_stream_positions.end()) {
        //No read attempt has been made yet for the stream.
        //First we have to find the data-chunk:
        try {
            auto chunk = this->findChunk(0x61746164); //"data" in little endian ("atad")
            if (chunk.first > -1) {
                //We found the data chunk: read from it
                size_t position = chunk.first + chunk.second.size;
                auto data = resource_ptr->read(bytes, position);
                position += data.size();
                //Remember the current position so that the data chunk
                //position doesn't need to be retrieved every time we want
                //to read something out of the RIFF file:
                this->audio_stream_positions[audio_stream_id] = position;
                return data;
            } else {
                //No data chunk: Nothing to read.
                return {};
            }
        } catch (Exception& e) {
            //Nothing to read:
            return {};
        }
    } else {
        //A read attempt has already been made on the chunk.
        auto data = resource_ptr->read(bytes, stream_position_it->second);
        stream_position_it->second += data.size();
        return data;
    }
    return {};
}


void RIFFContainer::writeAudioData(uint8_t audio_stream_id, const std::vector<uint8_t>& data)
{
    if (data.size() == 0) {
        return;
    }
    if (!this->header_written) {
        this->writeHeader();
    }
    auto written_data_it = this->audio_stream_written_data.find(audio_stream_id);
    if (written_data_it == this->audio_stream_written_data.end()) {
        //Invalid stream! The written data counter should have been initialised
        //for the stream when writing the header!
        return;
    }
    if (!this->resource.expired()) {
        auto resource_ptr = this->resource.lock();
        if (resource_ptr != nullptr) {
            //TODO: check file position so that we don't overwrite other data!
            written_data_it->second += resource_ptr->write(data);
        }
    }
}


std::vector<uint8_t> RIFFContainer::readVideoData(uint8_t video_stream_id, size_t bytes)
{
    //Not yet implemented.
    return {};
}

void RIFFContainer::writeVideoData(uint8_t video_stream_id, const std::vector<uint8_t>& data)
{
    //Not yet implemented.
}
