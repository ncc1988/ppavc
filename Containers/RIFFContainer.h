/*
    RIFFContainer.h
    This file is part of ppavc - planned program for audio and video conversion.
    Copyright (C) 2016 - 2022 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef PPAVC__RIFFCONTAINER_H
#define PPAVC__RIFFCONTAINER_H


/*
 * Resources:
 * https://en.wikipedia.org/wiki/Resource_Interchange_File_Format
 */


#include <endian.h>
#include <map>

#include <fmt/core.h>

#include "Container.h"
#include "../Core/Exceptions.h"


namespace PPAVC
{
    /**
     * The RIFFType enum class specifies the type of a RIFF container.
     */
    enum class RIFFType
    {
        /**
         * Unspecified or unknown RIFF type.
         */
        UNSPECIFIED,

        /**
         * RIFF WAVE container: audio only.
         */
        WAVE,

        /**
         * RIFF AVI container: audio and video.
         */
        AVI
    };



    /**
     * RIFFParentChunk represents RIFF chunks that can have sub-chunks.
     */
    class RIFFParentChunk
    {
        public:


        /**
         * The chunk-ID.
         */
        uint32_t id = 0;


        /**
         * The chunk size in bytes.
         */
        uint32_t size = 0;


        /**
         * The form type (RIFF chunk) or the list type (LIST chunk).
         */
        uint32_t type = 0;
    };


    /**
     * RIFFChunk represents ordinary RIFF chunks that cannot have
     * sub-chunks.
     */
    class RIFFChunk
    {
        public:


        /**
         * The chunk-ID.
         */
        uint32_t id = 0;


        /**
         * The chunk size in bytes.
         */
        uint32_t size = 0;


        /**
         * Determines whether the RIFFChunk instance is uninitialised.
         * This is the case when id and size are both zero.
         *
         * @returns bool True, if the RIFFChunk is unitialised, false otherwise.
         */
        bool isNull()
        {
            return id == size == 0;
        }
    };


    /**
     * RIFFWaveFormatData represents the WAVE format chunk content
     * that holds all relevant information to describe the WAVE data
     * (bitrate, channels and more).
     */
    class RIFFWaveFormatData
    {
        public:


        uint16_t format = 0;


        /**
         * The amount of audio channels.
         */
        uint16_t audio_channels = 0;


        /**
         * The sample rate.
         */
        uint32_t sample_rate = 0;


        /**
         * The amount of bytes per second.
         */
        uint32_t bytes_per_second = 0;


        /**
         * The amount of bytes per sample.
         */
        uint16_t bytes_per_sample = 0;


        /**
         * The amount of bits per sample. This is bytes_per_sample * 8.
         */
        uint16_t bits_per_sample = 0;


        /**
         * The default constructor.
         */
        RIFFWaveFormatData()
        {
            //No special code needed for the default constructor.
        }


        /**
         * A second constructor to set all fields automatically by
         * four provided values.
         *
         * @param uint16_t format The WAVE format.
         *
         * @param uint16_t audio_channels The amount of audio channels.
         *
         * @param uint32_t sample_rate The sample rate of the WAVE data.
         *
         * @param uint32_t bits_per_sample The amount of bits per sample.
         */
        RIFFWaveFormatData(
            uint16_t format,
            uint16_t audio_channels,
            uint32_t sample_rate,
            uint32_t bits_per_sample
            )
            : format(format),
              audio_channels(audio_channels),
              sample_rate(sample_rate),
              bits_per_sample(bits_per_sample)
        {
            this->bytes_per_sample = this->bits_per_sample / 8;
            this->bytes_per_second = this->sample_rate * this->bytes_per_sample * this->audio_channels;
        }
    };


    /**
     * This Container implementation handles RIFF multimedia containers.
     */
    class RIFFContainer: public Container
    {
        protected:


        /**
         * The type of the RIFF container.
         */
        RIFFType riff_type;


        /**
         * A helper method to read all RIFF chunks from a RIFF container.
         */
        void readChunkHeaders();


        /**
         * A helper method to read one RIFF chunk from a specified position.
         *
         * @param size_t position The position from where to read a RIFF chunk.
         *
         * @returns bool True, if a RIFF chunk could be read, false otherwise.
         */
        bool readChunk(size_t position);


        /**
         * This attribute holds the position where the next RIFF chunk
         * is expected to start.
         */
        uint64_t next_chunk_position;


        /**
         * This flag indicates whether a header has already been written (true)
         * or not (false). It is necessary to trigger writing the header
         * on the first writing of data for a stream.
         */
        bool header_written = false;


        /**
         * Returns the position of the chunk and the chunk itself
         * with the specified ID.
         */
        std::pair<size_t, RIFFChunk> findChunk(uint32_t id);


        /**
         * A map with all RIFF chunks. The first item is the position
         * of the chunk in the resource.
         */
        std::map<size_t, RIFFChunk> chunk_list;


        /**
         * Stores the position of the last read/write operation
         * for each audio stream.
         */
        std::map<uint8_t, size_t> audio_stream_positions;


        /**
         * Keeps track of the written data for each audio stream.
         * These information are needed to update the header
         * after all stream data has been written.
         */
        std::map<uint8_t, size_t> audio_stream_written_data;


        public:


        /**
         * @see Container::Container
         */
        RIFFContainer(std::shared_ptr<Resource> resource);


        /**
         * @see Container::readHeader
         */
        virtual void readHeader() override;


        /**
         * @see Container::writeHeader
         */
        virtual void writeHeader() override;


        /**
         * @see Container::updateHeader
         */
        virtual void updateHeader() override;


        /**
         * @see Container::readAudioData
         */
        virtual std::vector<uint8_t> readAudioData(uint8_t audio_stream_id = 0, size_t bytes = 65536) override;


        /**
         * @see Container::writeAudioData
         */
        virtual void writeAudioData(uint8_t audio_stream_id, const std::vector<uint8_t>& data) override;


        /**
         * @see Container::readVideoData
         */
        virtual std::vector<uint8_t> readVideoData(uint8_t video_stream_id = 0, size_t bytes = 65536) override;


        /**
         * @see Container::writeVideoData
         */
        virtual void writeVideoData(uint8_t video_stream_id, const std::vector<uint8_t>& data) override;
    };
}


#endif
