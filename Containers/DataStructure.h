/*
    DataStructure.h
    This file is part of endemm - encoder and decoder for multimedia.
    Copyright (C) 2016 - 2024 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef ENDEMM__DATASTRUCTURE_H
#define ENDEMM__DATASTRUCTURE_H


#include <memory>


#include "../Core/Resource.h"


namespace Endemm
{
    /**
     * The DataStructure interface provides methods for data structures
     * that are read from or written to a resource. Data structures in
     * endemm context mean records that have elements in a certain order
     * which has to be maintained when reading or writing them.
     *
     * DataStructure implementations are most likely format header elements
     * most of the time.
     *
     * Since DataStructure implementations represent a convenient way to
     * access records, their attributes shall be public by default for easy read
     * and write access. Conversions that are necessary to pack or unpack
     * the raw data shall be placed in the readFromResource or writeToResource
     * method implementations.
     */
    class DataStructure
    {
        public:

        /**
         * Reads the data structure from a resource.
         *
         * @param std::shared_ptr<Resource> resource The resource to read from.
         *
         * @returns bool True, if the reading was successful, false otherwise.
         */
        virtual bool readFromResource(std::shared_ptr<Resource> resource) = 0;

        /**
         * Writes the data structure to a resource.
         *
         * @param std::shared_ptr<Resource> resource The resource to write to.
         *
         * @returns bool True, if the writing was successful, false otherwise.
         */
        virtual bool writeToResource(std::shared_ptr<Resource> resource) = 0;

        /**
         * Calculates the size of the data in bytes that are read/written.
         * This may not be constant depending on the data structure:
         * During reading, there may be additional data that have to be read
         * and there may be additional data that are only there in specified
         * circumstances.
         *
         * @returns The size of the data structure.
         */
        virtual size_t getDataSize() = 0;
    };
}


#endif
