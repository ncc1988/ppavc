/*
    Chunk.h
    This file is part of endemm - encoder and decoder for multimedia.
    Copyright (C) 2016 - 2024 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef ENDEMM__RIFF__CHUNK_H
#define ENDEMM__RIFF__CHUNK_H


#include <endian.h>


#include "../../DataStructure.h"


namespace Endemm::RIFF
{
    /**
     * RIFF::Chunk represents ordinary RIFF chunks that cannot have
     * sub-chunks.
     */
    class Chunk : public DataStructure
    {
        public:


        /**
         * The chunk-ID.
         */
        uint32_t id = 0;


        /**
         * The chunk size in bytes.
         */
        uint32_t size = 0;

        public:

        /**
         * @see DataStructure::readFromResource
         */
        virtual bool readFromResource(std::shared_ptr<Resource> resource) override;

        /**
         * @see DataStructure::writeToResource
         */
        virtual bool writeToResource(std::shared_ptr<Resource> resource) override;

        /**
         * @see DataStructure::getDataSize
         */
        virtual size_t getDataSize() override;

        /**
         * Determines if the RIFF::Chunk instance is uninitialised.
         * This is the case when id and size are both zero.
         *
         * @returns bool True, if the instance is unitialised, false otherwise.
         */
        virtual bool isNull();
    };
}


#endif
