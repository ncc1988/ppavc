/*
    ParentChunk.cpp
    This file is part of endemm - encoder and decoder for multimedia.
    Copyright (C) 2016 - 2024 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "ParentChunk.h"


using namespace Endemm::RIFF;


bool ParentChunk::readFromResource(std::shared_ptr<Resource> resource)
{
    if (resource == nullptr) {
        return false;
    }

    std::vector<uint32_t> data(3);
    if (resource->readObject(reinterpret_cast<uint8_t*>(data.data()), 12)) {
        //NOTE: id and type contain text, so they are not converted to
        //host endiannes.
        this->id = data[0];
        this->size = le32toh(data[1]);
        this->type = data[2];
        return true;
    }
    return false;
}


bool ParentChunk::writeToResource(std::shared_ptr<Resource> resource)
{
    if (resource == nullptr) {
        return false;
    }

    //NOTE: id and type contain text, so they are not converted from
    //host endiannes.
    std::vector<uint32_t> data = {
        this->id,
        htole32(this->size),
        this->type
    };
    return resource->writeObject(reinterpret_cast<uint8_t*>(data.data()), 12);
}


size_t ParentChunk::getDataSize()
{
    //RIFF parent chunks are always 12 bytes long:
    return 12;
}


bool ParentChunk::isNull()
{
    return this->id == this->size == this->type == 0;
}
