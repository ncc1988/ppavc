/*
    Chunk.cpp
    This file is part of endemm - encoder and decoder for multimedia.
    Copyright (C) 2016 - 2024 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "ParentChunk.h"


using namespace Endemm::RIFF;


bool Chunk::readFromResource(std::shared_ptr<Resource> resource)
{
    if (resource == nullptr) {
        return false;
    }

    std::vector<uint32_t> data(2);
    if (resource->readObject(reinterpret_cast<uint8_t*>(data.data()), 8)) {
        //NOTE: id contains text, so it is not converted to
        //host endiannes.
        this->id = data[0];
        this->size = le32toh(data[1]);
        return true;
    }
    return false;
}


bool Chunk::writeToResource(std::shared_ptr<Resource> resource)
{
    if (resource == nullptr) {
        return false;
    }

    //NOTE: id contains text, so it is not converted from
    //host endiannes.
    std::vector<uint32_t> data = {
        this->id,
        htole32(this->size)
    };
    return resource->writeObject(reinterpret_cast<uint8_t*>(data.data()), 8);
}


size_t Chunk::getDataSize()
{
    //RIFF chunks are always 8 bytes long:
    return 8;
}


bool Chunk::isNull()
{
    return this->id == this->size == 0;
}
