/*
    WaveFormatData.cpp
    This file is part of endemm - encoder and decoder for multimedia.
    Copyright (C) 2016 - 2024 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "WaveFormatData.h"


using namespace Endemm::RIFF;


bool WaveFormatData::readFromResource(std::shared_ptr<Resource> resource)
{
    if (resource == nullptr) {
        return false;
    }
    std::vector<uint16_t> data(8);

    if (resource->readObject(reinterpret_cast<uint8_t*>(data.data()), 16)) {
        this->format = le16toh(data[0]);
        this->audio_channels = le16toh(data[1]);
        this->sample_rate = le32toh(data[3] << 16 | data[2]);
        this->bytes_per_second = le32toh(data[5] << 16 | data[4]);
        this->bytes_per_sample = le16toh(data[6]);
        this->bits_per_sample = le16toh(data[7]);
        return true;
    }
    return false;
}

bool WaveFormatData::writeToResource(std::shared_ptr<Resource> resource)
{
    if (resource == nullptr) {
        return false;
    }
    std::vector<uint16_t> data = {
        htole16(this->format),
        htole16(this->audio_channels),
        htole16(this->sample_rate & 0x0000ffff),
        htole16(this->sample_rate >> 16 & 0x0000ffff),
        htole16(this->bytes_per_second & 0x0000ffff),
        htole16(this->bytes_per_second >> 16 & 0x0000ffff),
        htole16(this->bytes_per_sample),
        htole16(this->bits_per_sample)
    };
    return resource->writeObject(reinterpret_cast<uint8_t*>(data.data()), 16);
}

size_t WaveFormatData::getDataSize()
{
    //Supported WAVE format data headers are 16 bytes long.
    return 16;
}


WaveFormatData::WaveFormatData()
{
    //No special code needed for the default constructor.
}


WaveFormatData::WaveFormatData(
    uint16_t format,
    uint16_t audio_channels,
    uint32_t sample_rate,
    uint32_t bits_per_sample
    )
    : format(format),
      audio_channels(audio_channels),
      sample_rate(sample_rate),
      bits_per_sample(bits_per_sample)
{
    this->bytes_per_sample = this->bits_per_sample / 8;
    this->bytes_per_second = this->sample_rate * this->bytes_per_sample * this->audio_channels;
}
