/*
    WaveFormatData.h
    This file is part of endemm - encoder and decoder for multimedia.
    Copyright (C) 2016 - 2024 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef ENDEMM__RIFF__WAVEFORMATDATA_H
#define ENDEMM__RIFF__WAVEFORMATDATA_H


#include <endian.h>


#include "../../DataStructure.h"


namespace Endemm::RIFF
{
    /**
     * RIFFWaveFormatData represents the WAVE format chunk content
     * that holds all relevant information to describe the WAVE data
     * (bitrate, channels and more).
     */
    class WaveFormatData: public DataStructure
    {
        public:


        uint16_t format = 0;


        /**
         * The amount of audio channels.
         */
        uint16_t audio_channels = 0;


        /**
         * The sample rate.
         */
        uint32_t sample_rate = 0;


        /**
         * The amount of bytes per second.
         */
        uint32_t bytes_per_second = 0;


        /**
         * The amount of bytes per sample.
         */
        uint16_t bytes_per_sample = 0;


        /**
         * The amount of bits per sample. This is bytes_per_sample * 8.
         */
        uint16_t bits_per_sample = 0;

        public:

        /**
         * @see DataStructure::readFromResource
         */
        virtual bool readFromResource(std::shared_ptr<Resource> resource) override;

        /**
         * @see DataStructure::writeToResource
         */
        virtual bool writeToResource(std::shared_ptr<Resource> resource) override;

        /**
         * @see DataStructure::getDataSize
         */
        virtual size_t getDataSize() override;

        /**
         * The default constructor.
         */
        WaveFormatData();


        /**
         * A second constructor to set all fields automatically by
         * four provided values.
         *
         * @param uint16_t format The WAVE format.
         *
         * @param uint16_t audio_channels The amount of audio channels.
         *
         * @param uint32_t sample_rate The sample rate of the WAVE data.
         *
         * @param uint32_t bits_per_sample The amount of bits per sample.
         */
        WaveFormatData(
            uint16_t format,
            uint16_t audio_channels,
            uint32_t sample_rate,
            uint32_t bits_per_sample
            );
    };
}


#endif
