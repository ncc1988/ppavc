/*
    Container.h
    This file is part of ppavc - planned program for audio and video conversion.
    Copyright (C) 2016 - 2022 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef PPAVC__CONTAINER_H
#define PPAVC__CONTAINER_H


#include <inttypes.h>
#include <iostream>
#include <map>
#include <memory>
#include <vector>


#include "../Core/Resource.h"
#include "../Core/AudioStreamConfig.h"
#include "../Core/VideoStreamConfig.h"


namespace PPAVC
{
    /**
     * A Container is a multimedia data container that is attached
     * to a resource from which it reads stream data or to which it
     * writes stream data.
     *
     * Implementations of the Container class represent a multimedia
     * container format.
     */
    class Container
    {
        protected:


        /**
         * The resource on which this container operates on.
         */
        std::weak_ptr<Resource> resource;


        /**
         * This map holds the information which audio stream number
         * contains which audio codec. The codec is represented by a string.
         */
        std::map<uint8_t, std::string> audio_streams;


        /**
         * This map holds the audio stream configuration for each audio stream.
         */
        std::map<uint8_t, AudioStreamConfig> audio_stream_configs;


        /**
         * This map holds the information which video stream number
         * contains which video codec. The codec is represented by a string.
         */
        std::map<uint8_t, std::string> video_streams;


        /**
         * This map holds the video stream configuration for each video stream.
         */
        std::map<uint8_t, VideoStreamConfig> video_stream_configs;


        public:


        /**
         * The constructor of a container needs a resource as parameter since
         * a container without a resource is not useful and a container is
         * bound to a resource.
         *
         * @param std::shared_ptr<Resource> resource The resource this container
         *     is bound to.
         */
        Container(std::shared_ptr<Resource> resource)
            : resource(resource)
        {
            //Nothing else at the moment.
        }


        /**
         * Returns all detected audio stream IDs and their codecs.
         *
         * @returns A map with the audio streams. The codec is represented
         *     by a string.
         */
        virtual std::map<uint8_t, std::string> getAudioStreams();


        /**
         * Returns all detected video stream IDs and their codecs.
         *
         * @returns A map with the video streams. The codec is represented
         *     by a string.
         */
        virtual std::map<uint8_t, std::string> getVideoStreams();


        /**
         * This method returns the codec for a specified audio stream or the first
         * audio stream, if no stream-ID is set.
         *
         * @returns std::string The name of the codec for the audio stream.
         *     The string is empty if no such stream exists.
         **/
        virtual std::string getAudioCodec(uint8_t audio_stream_id = 0);


        /**
         * This method sets the codec for an audio stream.
         */
        virtual void setAudioCodec(uint8_t audio_stream_id, const std::string& codec);


        /**
         * Returns the audio stream configuration for a specified audio stream.
         *
         * @param uint8_t audio_stream_id The ID of the audio stream.
         *
         * @returns AudioStreamConfig The configuration for the audio stream.
         */
        virtual AudioStreamConfig getAudioStreamConfig(uint8_t audio_stream_id = 0);


        /**
         * Sets the audio stream configuration for a specified audio stream.
         *
         * @param uint8_t audio_stream_id The stream-ID for which to set
         *     the configuration.
         *
         * @param const AudioStreamConfig& config The configuration for the
         *     audio stream.
         */
        virtual void setAudioStreamConfig(uint8_t audio_stream_id, const AudioStreamConfig& config);


        /**
         * This method returns the codec for a specified video stream or the first
         * video stream, if no stream-ID is set.
         *
         * @returns std::string The name of the codec for the video stream.
         *     The string is empty if no such stream exists.
         **/
        virtual std::string getVideoCodec(uint8_t video_stream_id = 0);


        /**
         * This method sets the codec for a video stream.
         */
        virtual void setVideoCodec(uint8_t video_stream_id, const std::string& codec);


        /**
         * Returns the video stream configuration for a specified video stream.
         *
         * @param uint8_t video_stream_id The ID of the video stream.
         *
         * @returns VideoStreamConfig The configuration for the video stream.
         */
        virtual VideoStreamConfig getVideoStreamConfig(uint8_t video_stream_id = 0);


        /**
         * Returns the resource this container is bound to.
         *
         * @returns std::shared_ptr<Resource> The resource for this container.
         *     If the resource is unavailable, a nullptr is returned.
         */
        virtual std::shared_ptr<Resource> getResource()
        {
            if (this->resource.expired()) {
                return nullptr;
            }
            return this->resource.lock();
        };


        /**
         * Method to read the header data of a multimedia container.
         */
        virtual void readHeader() = 0;


        /**
         * Writes a header to the resource.
         */
        virtual void writeHeader() = 0;


        /**
         * Updates the header. This method can be useful in implementations
         * to update the header after all stream data has been written.
         */
        virtual void updateHeader() = 0;


        /**
         * Reads an amount of bytes from a certain audio stream.
         *
         * @param uint8_t audio_stream_id The stream to read data from.
         *
         * @param size_t bytes The amount of bytes to read.
         *
         * @returns std::vector<uint8_t> The read data. The vector is empty,
         *     if no data could be read.
         */
        virtual std::vector<uint8_t> readAudioData(uint8_t audio_stream_id = 0, size_t bytes = 65536) = 0;


        /**
         * Writes an amount of bytes onto a certain audio stream.
         *
         * @param uint8_t audio_stream_id The stream to write data to.
         *
         * @param const std::vector<uint8_t>& data The data to write.
         */
        virtual void writeAudioData(uint8_t audio_stream_id, const std::vector<uint8_t>& data) = 0;


        /**
         * Reads an amount of bytes from a certain video stream.
         *
         * @param uint8_t video_stream_id The stream to read data from.
         *
         * @param size_t bytes The amount of bytes to read.
         *
         * @returns std::vector<uint8_t> The read data. The vector is empty,
         *     if no data could be read.
         */
        virtual std::vector<uint8_t> readVideoData(uint8_t video_stream_id = 0, size_t bytes = 65536) = 0;


        /**
         * Writes an amount of bytes onto a certain video stream.
         *
         * @param uint8_t video_stream_id The stream to write data to.
         *
         * @param const std::vector<uint8_t>& data The data to write.
         */
        virtual void writeVideoData(uint8_t video_stream_id, const std::vector<uint8_t>& data) = 0;
    };
}


#endif
