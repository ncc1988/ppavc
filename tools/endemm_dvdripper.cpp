/*
    endemm - encoder and decoder for multimedia
    Copyright (C) 2016 - 2024 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <iostream>
#include <memory>
#include <string>
#include <vector>


#include "../Core/DVDResource.h"
#include "../Core/FileResource.h"


using namespace Endemm;


void printHelp()
{
    std::cerr << "Usage: endemm_dvdripper OUTPUT_FILE DVD_TITLE [DVD_DEVICE]\n\n"
              << "\tOUTPUT_FILE is the file where the title shall be written to.\n"
              << "\tDVD_TITLE is the number of the title that shall be ripped.\n"
              << "\tDVD_DEVICE is the optional name of the DVD device."
              << std::endl;
}


int main(int argc, char** argv)
{
    std::vector<std::string> args(argv + 1, argv + argc);

    if (args.size() < 2) {
        printHelp();
        return 0;
    }

    auto file_name = args[0];
    if (file_name.empty()) {
        std::cerr << "The output file name is invalid." << std::endl;
        return 1;
    }

    auto title = args[1];
    if (title.empty()) {
        std::cerr << "The title is invalid." << std::endl;
        return 1;
    }
    std::string dvd_device = "";
    if (args.size() == 3) {
        dvd_device = args[2];
    }

    auto file_resource = std::make_unique<FileResource>(file_name, true);
    if (file_resource == nullptr) {
        std::cerr << "Cannot open the output file for writing." << std::endl;
        return 1;
    }

    auto dvd_resource = std::make_unique<DVDResource>(title);
    if (dvd_resource == nullptr) {
        std::cerr << "Cannot initialise the DVD resource." << std::endl;
        return 1;
    }

    if (!dvd_device.empty()) {
        dvd_resource->setDVDDevice(dvd_device);
    }

    if(!dvd_resource->initialise()) {
        std::cerr << "Cannot initialise reading data from the DVD." << std::endl;
        return 1;
    }

    //$read_size holds the reading size in KiB (1024 bytes).
    //Since a sector on a video DVD is 2048 bytes in size, the amount of
    //sectors to read just needs to be divided by 2.
    uint64_t read_size = dvd_resource->getEndSector() - dvd_resource->getStartSector();
    std::cerr << "Reading data from sector " << dvd_resource->getStartSector()
              << " to " << dvd_resource->getEndSector()
              << " (" << read_size << " sectors)\n";

    size_t i = 0;
    size_t block_size = 256 * 1024; //256 KiB
    size_t written_bytes = 0;
    std::vector<uint8_t> data;
    do {
        data = dvd_resource->read(block_size);
        if (!data.empty()) {
            try {
                size_t currently_written = file_resource->write(data);
                if (currently_written != data.size()) {
                    std::cerr << "Error writing data." << std::endl;
                    return 1;
                }
                written_bytes += currently_written;
            } catch (Exception e) {
                std::cerr << "Cannot write file: " << e.getMessage() << std::endl;
                return 1;
            }
            if (i % 10 == 0) {
                //Output the progress:
                //std::cerr << "\r" << (written_bytes / 1024) << " / " << read_size << " KiB";
                std::cerr << "\rCurrent sector: " << dvd_resource->getCurrentSector();
            }
            i++;
        }
    } while (!data.empty());

    std::cerr << "\n";

    std::cerr << fmt::format(
        "End of data on sector {0}. {1} KiB read from DVD",
        dvd_resource->getCurrentSector(),
        written_bytes / 1024
        ) << std::endl;

    return 0;
}
