/*
    VideoCodec.h
    This file is part of ppavc - planned program for audio and video conversion.
    Copyright (C) 2016 - 2022 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef PPAVC__VIDEOCODEC_H
#define PPAVC__VIDEOCODEC_H


#include "Codec.h"
#include "../Core/VideoChainObject.h"
#include "../Core/VideoStreamConfig.h"



namespace PPAVC
{
    /**
     * The base class for all video codecs.
     */
    class VideoCodec: public Codec, public VideoChainObject
    {
        protected:


        /**
         * The configuration of the video codec.
         */
        VideoStreamConfig config;


        public:


        /**
         * Sets the configuration of the codec.
         *
         * @param const VideoStreamConfig& config The new configuration.
         */
        void setConfig(const VideoStreamConfig& config)
        {
            this->config = config;
        }


        /**
         * @returns VideoStreamConfig The current codec configuration.
         */
        VideoStreamConfig getConfig()
        {
            return this->config;
        }
    };
}


#endif
