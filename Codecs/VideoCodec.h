/*
    VideoCodec.h
    This file is part of endemm - encoder and decoder for multimedia.
    Copyright (C) 2016 - 2023 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef ENDEMM__VIDEOCODEC_H
#define ENDEMM__VIDEOCODEC_H


#include "Codec.h"
#include "../Core/VideoChainObject.h"
#include "../Core/VideoStreamConfig.h"



namespace Endemm
{
    /**
     * The base class for all video codecs.
     */
    class VideoCodec: public Codec, public VideoChainObject
    {
        protected:


        /**
         * The configuration of the video codec.
         */
        VideoStreamConfig config;


        public:


        VideoCodec();


        VideoCodec(const VideoStreamConfig& config, bool encode_mode = false);


        /**
         * Sets the configuration of the codec.
         *
         * @param const VideoStreamConfig& config The new configuration.
         */
        void setConfig(const VideoStreamConfig& config)
        {
            this->config = config;
        }


        /**
         * @returns VideoStreamConfig The current codec configuration.
         */
        VideoStreamConfig getConfig()
        {
            return this->config;
        }

        /**
         * @see Codec::getName
         */
        virtual std::string getName() override;

        /**
         * @see Codec::step
         */
        virtual bool step(size_t size) override;

        /**
         * TODO: move into Codec class.
         *
         * This is a helper method to read video data from the container
         * so that VideoCodec implementations do not need to reinvent the
         * wheel on which they get their data.
         *
         * @param size_t size The amount of bytes that shall be read.
         */
        virtual std::vector<uint8_t> readData(size_t size);


        /**
         * TODO: move into Codec class.
         *
         * This is a helper method to write data to the container
         * so that VideoCodec implementations do not need to reinvent the
         * wheel on which they put video data.
         *
         * @param const std::vector<uint8_t>& data The data that shall
         *     be written.
         *
         * @param bool last_chunk Whether data is the last chunk of data (true)
         *     or not (false).
         */
        virtual void writeData(const std::vector<uint8_t>& data, bool last_chunk = false);

        //VideoChainObject implementation:

        /**
         * @see VideoChainObject::getInputColourSpace
         */
        virtual std::vector<VideoColourSpace> getInputColourSpaces() override;

        /**
         * @see VideoChainObject::getOutputColourSpace
         */
        virtual std::vector<VideoColourSpace> getOutputColourSpace() override;

        //ChainObject implementation:

        /**
         * @see ChainObject::sendDataToEndpoint
         */
        virtual void sendDataToEndpoint(std::vector<uint8_t> data, bool last_chunk = false) override;

        /**
         * This implementation of the processData method just passes
         * video data through.
         *
         * @see ChainObject::processData
         */
        virtual std::vector<uint8_t> processData(std::vector<uint8_t> input) override;
    };
}


#endif
