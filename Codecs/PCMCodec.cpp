/*
    PCMCodec.cpp
    This file is part of ppavc - planned program for audio and video conversion.
    Copyright (C) 2016 - 2022 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "PCMCodec.h"


using namespace PPAVC;


std::string PCMCodec::getName()
{
    return "PCM";
}


std::vector<AudioIOFormat> PCMCodec::getInputFormats()
{
    return {AudioIOFormat::PCM_SAMPLES};
}


std::vector<AudioIOFormat> PCMCodec::getOutputFormats()
{
    return {AudioIOFormat::PCM_SAMPLES};
}


std::vector<uint8_t> PCMCodec::processData(std::vector<uint8_t> input)
{
    //Very simple, isn't it?
    return input;
}
