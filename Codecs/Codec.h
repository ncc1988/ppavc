/*
    Codec.h
    This file is part of ppavc - planned program for audio and video conversion.
    Copyright (C) 2016 - 2022 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef PPAVC__CODEC_H
#define PPAVC__CODEC_H


#include <memory>
#include <string>
#include <vector>


#include "../Containers/Container.h"


namespace PPAVC
{
    /**
     * The base class for all codecs.
     */
    class Codec
    {
        protected:


        /**
         * Whether the codec is in encoding mode (true) or decoding mode (false).
         * Defaults to false.
         */
        bool encode_mode = false;


        /**
         * The ID of the stream in the container the codec is attached to.
         */
        uint8_t stream_id = 0;


        /**
         * The container the codec is attached to.
         */
        std::weak_ptr<Container> container;


        public:


        /**
         * @returns The codecs name, which should be constant.
         */
        virtual std::string getName() = 0;


        /**
         * Sets the codec to encoding (compression) mode.
         */
        virtual void setEncodeMode()
        {
            this->encode_mode = true;
        }


        /**
         * Sets the codec to decoding (decompression) mode.
         * This is the default state.
         */
        virtual void setDecodeMode()
        {
            this->encode_mode = false;
        }


        /**
         * The stream-ID the codec has in the container it is attached to.
         */
        virtual void setStreamId(uint8_t stream_id)
        {
            this->stream_id = stream_id;
        }


        /**
         * @returns uint8_t The stream-ID the codec has in the container
         *     it is attached to.
         */
        virtual uint8_t getStreamId()
        {
            return this->stream_id;
        }


        virtual void setContainer(std::shared_ptr<Container> container)
        {
            this->container = container;
        }


        virtual std::shared_ptr<Container> getContainer()
        {
            if (this->container.expired()) {
                return nullptr;
            }
            return this->container.lock();
        }
    };
}


#endif
