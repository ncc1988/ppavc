/*
    PCMCodec.h
    This file is part of ppavc - planned program for audio and video conversion.
    Copyright (C) 2016 - 2022 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef PPAVC__PCMCODEC_H
#define PPAVC__PCMCODEC_H


#include <memory>
#include <string>
#include <cstddef>


#include "AudioCodec.h"


namespace PPAVC
{

    /**
     * A codec for PCM audio.
     */
    class PCMCodec: public AudioCodec
    {
        public:


        PCMCodec()
            : AudioCodec()
        {
            //Nothing else.
        }


        PCMCodec(const AudioStreamConfig& config, bool encode_mode = false)
            : AudioCodec(config, encode_mode)
        {
            //Nothing else.
        }


        /**
         * @see PPAVC::Codec::getName
         */
        virtual std::string getName() override;


        /**
         * @see PPAVC::AudioCodec::getInputFormats
         */
        virtual std::vector<AudioIOFormat> getInputFormats() override;


        /**
         * @see PPAVC::AudioCodec::getOutputFormats
         */
        virtual std::vector<AudioIOFormat> getOutputFormats() override;


        /**
         * @see PPAVC::AudioChainObject::processData
         */
        virtual std::vector<uint8_t> processData(std::vector<uint8_t> input) override;
    };
}


#endif
