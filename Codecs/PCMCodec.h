/*
    PCMCodec.h
    This file is part of endemm - encoder and decoder for multimedia.
    Copyright (C) 2016 - 2023 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef ENDEMM__PCMCODEC_H
#define ENDEMM__PCMCODEC_H


#include <memory>
#include <string>
#include <cstddef>


#include "AudioCodec.h"


namespace Endemm
{

    /**
     * A codec for PCM audio.
     */
    class PCMCodec: public AudioCodec
    {
        public:


        PCMCodec()
            : AudioCodec()
        {
            //Nothing else.
        }


        PCMCodec(const AudioStreamConfig& config, bool encode_mode = false)
            : AudioCodec(config, encode_mode)
        {
            //Nothing else.
        }


        /**
         * @see Endemm::Codec::getName
         */
        virtual std::string getName() override;


        /**
         * @see Endemm::AudioCodec::getInputFormats
         */
        virtual std::vector<AudioIOFormat> getInputFormats() override;


        /**
         * @see Endemm::AudioCodec::getOutputFormats
         */
        virtual std::vector<AudioIOFormat> getOutputFormats() override;


        /**
         * @see Endemm::AudioChainObject::processData
         */
        virtual std::vector<uint8_t> processData(std::vector<uint8_t> input) override;
    };
}


#endif
