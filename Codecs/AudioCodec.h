/*
    AudioCodec.h
    This file is part of ppavc - planned program for audio and video conversion.
    Copyright (C) 2016 - 2022 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef PPAVC__AUDIOCODEC_H
#define PPAVC__AUDIOCODEC_H


#include "Codec.h"
#include "../Core/AudioChainObject.h"
#include "../Core/AudioStreamConfig.h"


namespace PPAVC
{
    /**
     * The base class for all audio codecs.
     */
    class AudioCodec: public Codec, public AudioChainObject
    {
        public:


        AudioCodec();


        AudioCodec(const AudioStreamConfig& config, bool encode_mode = false);


        /**
         * Initialises the conversion (implemenation dependent)
         * and does one step: read data from container, process it and
         * send it to the next chain object.
         *
         * @param size_t size The amount of data to read and process
         *     in this step.
         *
         * @returns bool Whether data can be processed (true) or not (false).
         */
        virtual bool step(size_t size);


        /**
         * This is a helper method to read audio data from the container
         * so that AudioCodec implementations do not need to reinvent the
         * wheel on which they get their data.
         *
         * @param size_t size The amount of bytes that shall be read.
         */
        virtual std::vector<uint8_t> readData(size_t size);


        /**
         * This is a helper method to write data to the container
         * so that AudioCodec implementations do not need to reinvent the
         * wheel on which they put audio data.
         *
         * @param const std::vector<uint8_t>& data The data that shall
         *     be written.
         *
         * @param bool last_chunk Whether data is the last chunk of data (true)
         *     or not (false).
         */
        virtual void writeData(const std::vector<uint8_t>& data, bool last_chunk = false);


        //AudioChainObject implementation:

        /**
         * @see AudioChainObject::sendDataToEndpoint
         */
        virtual void sendDataToEndpoint(std::vector<uint8_t> data, bool last_chunk = false) override;
    };
}


#endif
