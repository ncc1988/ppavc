# ppavc - planned program for audio and video conversion

## What is ppavc?

ppavc is a program for converting audio and video data.

## How is "ppavc" pronounced?

To prevent confusion with the english word "pee", ppavc should be pronounced
in german. For the english tongue, the pronounciation would be:
"pèh-pèh-ah-fau-tseh" ("pèh" = p with a long e that is pronounced more like
a short "e" than a long "e").
