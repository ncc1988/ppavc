/*
    AudioChannelConverter.cpp
    This file is part of ppavc - planned program for audio and video conversion.
    Copyright (C) 2016 - 2022 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "AudioChannelConverter.h"


using namespace PPAVC;


template<typename T> std::vector<T> AudioChannelConverter::convertChannels(std::vector<T> input)
{
    if (this->input_stream_config.channels > this->output_stream_config.channels) {
        //Mix the input channels to fewer output channels.
        if (this->output_stream_config.channels == 0) {
            return {};
        }
        float mix_factor = this->output_stream_config.channels / this->input_stream_config.channels;
        std::vector<T> output;
        T current_sample = 0;
        uint16_t channel_c = 0;
        for (auto sample: input) {
            current_sample += sample * mix_factor;
            channel_c++;
            if (channel_c >= this->input_stream_config.channels) {
                output.push_back(current_sample);
                current_sample = 0;
                channel_c = 0;
            }
        }
        return output;
    } else if (this->input_stream_config.channels == this->output_stream_config.channels) {
        //Nothing to do.
        return input;
    } else {
        //Divide the input channels to several output channels.
        //TODO
        return input;
    }
}


void AudioChannelConverter::setConfig(std::map<std::string, std::string> config)
{
    //This converter has no config.
}


std::map<std::string, std::string> AudioChannelConverter::getConfig()
{
    //This converter has no config.
    return {};
}


std::vector<AudioIOFormat> AudioChannelConverter::getInputFormats()
{
    return {AudioIOFormat::PCM_SAMPLES};
}


std::vector<AudioIOFormat> AudioChannelConverter::getOutputFormats()
{
    return {AudioIOFormat::PCM_SAMPLES};
}


std::vector<uint8_t> AudioChannelConverter::processData(std::vector<uint8_t> input)
{
    if (this->input_stream_config == this->output_stream_config) {
        //Both are equal.
        return input;
    }

    //Convert the sample rate:
    if (this->input_stream_config.bits_per_sample == 8) {
        if (this->output_stream_config.bits_per_sample == 16) {
            //Convert from 8 bit to 16 bit unsigned:
            /*
            std::vector<uint16_t> converted_samples;
            for (auto sample: input) {
                converted_samples.push_back(sample * 256);
            }
            */
        } else if (this->output_stream_config.bits_per_sample == 8) {
            //Directly convert channels:
            return this->convertChannels(input);
        }
        auto samples = input;
    } else if (this->input_stream_config.bits_per_sample == 16) {
        std::vector<uint16_t>current_samples16();
        /*
        this->current_sample_byte = 0;
        for (auto input_byte: input) {
            if (this->current_sample_byte == 0) {
                this->current_samples16.push_back(
                    input_byte << 8
                    );
                this->current_sample_byte++;
            } else {
                this->current_samples16.last() |= input_byte;
                this->current_sample_byte = 0;
            }
        }
        */
        std::vector<uint16_t> samples(input.begin(), input.end());
        auto result = this->convertChannels(samples);
        return std::vector<uint8_t>(result.begin(), result.end());
    }
    //Unhandled cases:
    return {};
}
