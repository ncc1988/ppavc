/*
    DataConverter.h
    This file is part of ppavc - planned program for audio and video conversion.
    Copyright (C) 2016 - 2022 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PPAVC__DATACONVERTER_H
#define PPAVC__DATACONVERTER_H


#include <map>
#include <string>


#include "../Core/AudioChainObject.h"
#include "../Core/VideoChainObject.h"


namespace PPAVC
{
    /**
     * A DataConverter is responsible for converting raw data from one
     * format to another. The DataConverter interface is divided into
     * AudioDataConverter, VideoDataConverter and TextDataConverter
     * interfaces.
     *
     * What DataConverters do:
     * For example, an AudioDataConverter may do the conversion
     * from stereo to mono audio or perform an FFT on PCM data.
     *
     * A VideoDataConverter may convert RGB video data to YUV420
     * or quantize the video data.
     *
     */
    class DataConverter
    {
        /**
         * Sets the configuration of the converter.
         *
         * @param std::map<std::string, std::string> The configuration
         *     as key-value pairs.
         */
        virtual void setConfig(std::map<std::string, std::string> config) = 0;


        /**
         * Returns the configuration of the converter.
         *
         * @returns std::map<std::string, std::string> The configuration
         *     as key-value pairs.
         */
        virtual std::map<std::string, std::string> getConfig() = 0;
    };


    /**
     * An AudioDataConverter converts audio data.
     */
    class AudioDataConverter: public DataConverter, public AudioChainObject
    {
        //Nothing else at the moment.
    };


    /**
     * A VideoDataConverter converts video data.
     */
    class VideoDataConverter: public DataConverter, public VideoChainObject
    {
        //Nothing else at the moment.
    };
}


#endif
