/*
    AudioChannelConverter.h
    This file is part of ppavc - planned program for audio and video conversion.
    Copyright (C) 2016 - 2022 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef PPAVC__AUDIOCHANNELCONVERTER_H
#define PPAVC__AUDIOCHANNELCONVERTER_H


#include "DataConverter.h"


namespace PPAVC
{
    /**
     * The AudioChannelConverter converts an audio stream with X audio
     * channels to an audio stream with Y audio channels. Common operations
     * are mixing a stereo stream to mono or to convert a mono stream to stereo.
     */
    class AudioChannelConverter: public AudioDataConverter
    {
        protected:


        template<typename T> std::vector<T> convertChannels(std::vector<T> input);


        public:


        /**
         * @see DataConverter::setConfig
         */
        virtual void setConfig(std::map<std::string, std::string> config) override;


        /**
         * @see DataConverter::getConfig
         */
        virtual std::map<std::string, std::string> getConfig() override;


        /**
         * @see AudioChainObject::getInputFormats
         */
        virtual std::vector<AudioIOFormat> getInputFormats() override;


        /**
         * @see AudioChainObject::getOutputFormats
         */
        virtual std::vector<AudioIOFormat> getOutputFormats() override;


        /**
         * @see AudioChainObject::processData
         */
        virtual std::vector<uint8_t> processData(std::vector<uint8_t> input) override;
    };
}


#endif
