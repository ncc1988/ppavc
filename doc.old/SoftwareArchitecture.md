# Classes

## ConversionManager

A ConversionManager manages Containers, Codecs and Processors
and controls the multimedia conversion as a whole.

[Implementation note: ConversionManager is not fully defined.
It may be implemented as singleton.]

## Container

A Container class handles multimedia container formats.
It handles stream data reading and writing and reading metadata.


[Implementation note: Container is not fully defined.
But it is very likely that a Container class MUST
support handling multiple threads!]

## Codec

A codec is responsible for decoding and encoding one stream.
If it decodes data it outputs them in a raw data format.
If it encodes data it gets them in a supported raw data format.

[Implementation note: Codec is not fully defined.
It is very likely that Codec isn't required to suport multiple threads
since a Codec instance will be created for each stream.]

## CodecConfiguration

A CodecConfiguration object is passed to a codec. Depending
on the type of codec the CodecConfiguration is of type
AudioCodecConfiguration, VideoCodecConfiguration or
TextCodecConfiguration.

The content of CodecConfiguration objects describes codec parameters
like bitrate, audio channels, video resolution, etc.

[Implementation note: A CodecConfiguration object is likely to be passed
from a ConversionManager instance that can read the codec's configuration from
the multimedia container header.

Note: The codec configuration is passed to the codec object using a pointer.
The codec object may use, manipulate and delete the configuration so it
is always wise to check if the configuration object still exists.
]

## Processor

A processor manipulates raw data and returns the modified data.

[Implementation note: Processor is not fully defined.
It isn't required to support multiple threads.]

