endemm requires the following libraries to be installed:

- fmt

endemm requires the following tools to be present:

- cmake
- a C++ compiler like g++ or clang that can compile modern C++ (C++17 or newer)

The following libaries are optional:

- dvdread (only when ENABLE_DVD_SUPPORT is set)
