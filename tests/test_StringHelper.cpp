/*
    test_StringHelper.cpp
    This file is part of endemm - encoder and decoder for multimedia.
    Copyright (C) 2016 - 2024 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <iostream>
#include <string>
#include <vector>


#include "../Core/Helpers/StringHelper.h"


int main()
{
    std::string test = "This is a test string.";
    auto tokens = Endemm::StringHelper::explode(" ", test, 3);
    if (tokens.size() != 4) {
        std::cerr << "\"This is a test string.\" split by \" \": Splitting the string did not result in the correct amount of tokens." << std::endl;
        return 1;
    }

    if ((tokens[0] != "This") || (tokens[1] != "is") || (tokens[2] != "a")
        || (tokens[3] != "test string.")) {
        std::cerr << "\"This is a test string.\" split by \" \": The tokens resulting from the split do not have the correct values." << std::endl;
        return 1;
    }

    tokens = Endemm::StringHelper::explode("test", test);
    if (tokens.size() != 2) {
        std::cerr << "\"This is a test string.\" split by \"test\": Splitting the string did not result in the correct amount of tokens." << std::endl;
        return 1;
    }

    if ((tokens[0] != "This is a ") || (tokens[1] != " string.")) {
        std::cerr << "\"This is a test string.\" split by \"test\": The tokens resulting from the split do not have the correct values." << std::endl;
        return 1;
    }

    tokens = Endemm::StringHelper::explode("://", "https://example.org", 1);
    if (tokens.size() != 2) {
        std::cerr << "\"https://example.org\" split by \"://\": Splitting the string did not result in the correct amount of tokens." << std::endl;
        return 1;
    }
    if ((tokens[0] != "https") || (tokens[1] != "example.org")) {
        std::cerr << "\"https://example.org\" split by \"://\": The tokens resulting from the split do not have the correct values." << std::endl;
        return 1;
    }

    return 0;
}
