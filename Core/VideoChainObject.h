/*
    VideoChainObject.h
    This file is part of endemm - encoder and decoder for multimedia.
    Copyright (C) 2016 - 2023 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef ENDEMM__VIDEOCHAINOBJECT_H
#define ENDEMM__VIDEOCHAINOBJECT_H


#include "ChainObject.h"
#include "VideoColourSpace.h"
#include "VideoStreamConfig.h"


namespace Endemm
{
    /**
     * A VideoChainObject is an extension of the ChainObject class
     * for video data conversion.
     */
    class VideoChainObject: public ChainObject
    {
        protected:

        VideoStreamConfig input_stream_config;

        VideoStreamConfig output_stream_config;

        public:

        /**
         * Returns the list of colour spaces that this codec
         * supports for input data (data to be encoded).
         *
         * If this list is empty, it means that the codec cannot
         * encode any data.
         */
        virtual std::vector<VideoColourSpace> getInputColourSpaces() = 0;


        /**
         * Returns the list of colour spaces that this codec
         * supports for output (decoded) data.
         *
         * If this list is empty, it means that the codec cannot
         * decode any data.
         */
        virtual std::vector<VideoColourSpace> getOutputColourSpace() = 0;
    };
}


#endif
