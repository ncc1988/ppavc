/*
    VideoChainObject.h
    This file is part of ppavc - planned program for audio and video conversion.
    Copyright (C) 2016 - 2022 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef PPAVC__VIDEOCHAINOBJECT_H
#define PPAVC__VIDEOCHAINOBJECT_H


#include "VideoColourSpace.h"
#include "VideoStreamConfig.h"


namespace PPAVC
{
    /**
     * A VideoChainObject implementation is a class whose instances
     * can be chained together to convert video data.
     */
    class VideoChainObject
    {
        /**
         * Returns the list of colour spaces that this codec
         * supports for input data (data to be encoded).
         *
         * If this list is empty, it means that the codec cannot
         * encode any data.
         */
        virtual std::vector<VideoColourSpace> getInputColourSpaces() = 0;


        /**
         * Returns the list of colour spaces that this codec
         * supports for output (decoded) data.
         *
         * If this list is empty, it means that the codec cannot
         * decode any data.
         */
        virtual std::vector<VideoColourSpace> getOutputColourSpace() = 0;
    };
}


#endif
