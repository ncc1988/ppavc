/*
    Exceptions.h
    This file is part of ppavc - planned program for audio and video conversion.
    Copyright (C) 2016 - 2022 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PPAVC__EXCEPTIONS_H
#define PPAVC__EXCEPTIONS_H


#include <string>


#include "StringConvertible.h"
#include "StreamID.h"


namespace PPAVC
{
    class Exception : StringConvertible
    {
        protected:


        std::string message;


        public:


        virtual std::string getMessage()
        {
            return this->message;
        }


        Exception(std::string message)
        {
            this->message = message;
        }


        virtual std::string toString() override
        {
            return this->getMessage();
        }
    };


    class FormatDetectionException: public Exception
    {
        public:


        FormatDetectionException(std::string message)
            : Exception(message)
        {
            //Nothing else
        }
    };


    class DataIOResourceException: public Exception
    {
        public:


        DataIOResourceException(std::string message)
            : Exception(message)
        {
            //Nothing else
        }
    };


    class MappingException: public Exception
    {
        protected:


        std::vector<std::pair<StreamID, std::string>> errors;


        public:


        MappingException(std::string message, std::vector<std::pair<StreamID, std::string>> errors)
            : Exception(message),
              errors(errors)
        {
            //Nothing else
        }


        virtual std::string toString() override
        {
            std::string result = Exception::toString();
            for (auto error: errors) {
                result += error.first.toString() + ": " + error.second;
            }
            return result;
        }
    };
}

#endif
