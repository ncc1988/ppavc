/*
    Resource.cpp
    This file is part of endemm - encoder and decoder for multimedia.
    Copyright (C) 2016 - 2023 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "Resource.h"


using namespace Endemm;


Resource::Resource(const std::string& location)
    : location(location)
{
    //Nothing else.
}


uint16_t Resource::getId()
{
    return this->id;
}


void Resource::setId(uint16_t id)
{
    this->id = id;
}


std::string Resource::getLocation()
{
    return this->location;
}


void Resource::setLocation(const std::string& location)
{
    this->location = location;
}


bool Resource::isReadable()
{
    return this->read_access;
}


bool Resource::isWritable()
{
    return this->write_access;
}


void Resource::setProber(std::shared_ptr<ResourceProber> prober)
{
    this->prober = prober;
}


std::shared_ptr<ResourceProber> Resource::getProber()
{
    return this->prober;
}


std::shared_ptr<Container> Resource::probe()
{
    if (this->prober == nullptr) {
        //Nothing to probe.
        return nullptr;
    }
    return this->prober->probeResource(this->shared_from_this());
}


std::string Resource::toString()
{
    return this->location;
}
