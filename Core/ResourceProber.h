/*
    ResourceProber.h
    This file is part of endemm - encoder and decoder for multimedia.
    Copyright (C) 2016 - 2024 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef ENDEMM__RESOURCEPROBER_H
#define ENDEMM__RESOURCEPROBER_H


#include <memory>


#include "Resource.h"
#include "../Containers/Container.h"


namespace Endemm
{
    //Due to circular inclusion,
    //the Container and Resource classes are declared here.
    class Container;
    class Resource;


    /**
     * A ResourceProber implementation is responsible for probing resources.
     * Its purpose is to find the correct container for a resource.
     * Different probers may be necessary depending on the kind of resource
     * that is being probed.
     */
    class ResourceProber
    {
        public:

        /**
         * This method probes the specified resource to find the correct
         * container for the content.
         * If no matching container can be found, a nullptr is returned.
         *
         * @param std::shared_ptr<Resource> resource The resource to probe.
         *
         * @returns std::shared_ptr<Container> The container for the resource
         *     or a nullptr in case no such container can be found.
         */
        virtual std::shared_ptr<Container> probeResource(
            std::shared_ptr<Resource> resource
            ) = 0;
    };
}


#endif
