/*
    StringHelper.h
    This file is part of endemm - encoder and decoder for multimedia.
    Copyright (C) 2016 - 2024 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "StringHelper.h"


using namespace Endemm;


std::vector<std::string> StringHelper::explode(
    const std::string separator,
    const std::string input,
    const uint32_t limit
    )
{
    if (separator.empty() || input.empty()) {
        //Nothing to split.
        return {};
    }
    uint32_t splits = 0;
    size_t last_position = 0;
    std::vector<std::string> tokens;
    size_t current_position = input.find(separator);
    while (current_position != std::string::npos
           && (((limit > 0) && (splits < limit)) || limit == 0)) {
        auto substring = input.substr(last_position, current_position - last_position);
        tokens.push_back(substring);
        last_position = current_position + separator.length();
        if (last_position > input.length()) {
            //The next iteration would go beyond the length of the input string.
            break;
        }
        splits++;
        current_position = input.find(separator, last_position);
    }
    if (last_position < input.length()) {
        //There are parts left at the end that have to be added:
        auto substring = input.substr(last_position);
        tokens.push_back(substring);
    }
    return tokens;
}
