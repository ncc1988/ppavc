/*
    ConversionManager.cpp
    This file is part of ppavc - planned program for audio and video conversion.
    Copyright (C) 2016 - 2022 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "ConversionManager.h"


using namespace PPAVC;


ConversionManager::ConversionManager()
{
    //Nothing at the moment.
}


ConversionManager::ConversionManager(std::string inputResourceURI, std::string outputResourceURI)
{
    // First we try to open the input resource.
    std::shared_ptr<FileResource> input_file = std::make_shared<FileResource>(inputResourceURI);

    if (input_file == nullptr) {
        throw Exception("Error initialising resource reading module!");
    }
    //If the constructor hasn't thrown an exception, we wan push the input
    //resource to the input list:
    this->inputs.push_back(input_file);

    if (!outputResourceURI.empty()) {
        std::shared_ptr<FileResource> output_file = std::make_shared<FileResource>(outputResourceURI);

        if (output_file == nullptr) {
            throw Exception("Error initialising resource writing module!");
        }

        //If the constructor hasn't thrown an exception, we wan push the output
        //resource to the output list:
        this->outputs.push_back(output_file);
    }
}


ConversionManager::~ConversionManager()
{
    //Nothing at the moment.
}


std::shared_ptr<Resource> ConversionManager::getInputResource(uint8_t resource_id)
{
    try {
        return this->inputs.at(resource_id);
    } catch (std::out_of_range& e) {
        return nullptr;
    }
}


std::shared_ptr<Resource> ConversionManager::getOutputResource(uint8_t resource_id)
{
    try {
        return this->outputs.at(resource_id);
    } catch (std::out_of_range& e) {
        return nullptr;
    }
}


void ConversionManager::addOutputResource(std::shared_ptr<Resource> resource)
{
    if (resource != nullptr) {
        this->outputs.push_back(resource);
    }
}


void ConversionManager::setMapping(
    std::vector<std::pair<StreamID, StreamID>> mapping,
    bool use_sequential_mapping
    )
{
    this->mapping = mapping;
    this->use_sequential_mapping = use_sequential_mapping;
}


std::vector<std::pair<StreamID, std::string>> ConversionManager::checkMapping()
{
    std::vector<std::pair<StreamID, std::string>> result;
    for (auto rule: this->mapping) {
        auto input_stream_id = rule.first;
        auto output_stream_id = rule.second;
        if (this->inputs.size() <= input_stream_id.resource_id) {
            //Invalid input resource ID.
            result.push_back(
                std::pair<StreamID, std::string>(
                    input_stream_id,
                    fmt::format(
                        "Stream {0}: No input resource available with the ID {1}.",
                        input_stream_id.toString(),
                        input_stream_id.resource_id
                        )
                    )
                );
        }
        if (this->outputs.size() <= output_stream_id.resource_id) {
            //Invalid output resource ID.
            result.push_back(
                std::pair<StreamID, std::string>(
                    output_stream_id,
                    fmt::format(
                        "In Stream {0}: No output resource available with the ID {1}.",
                        output_stream_id.toString(),
                        output_stream_id.resource_id
                        )
                    )
                );
        }
    }
    return result;
}


std::shared_ptr<Container> ConversionManager::probeResource(std::shared_ptr<Resource> resource)
{
    //Stub
    return std::make_shared<RIFFContainer>(resource);
}


std::vector<std::shared_ptr<Container>> ConversionManager::createOutputContainers()
{
    std::vector<std::shared_ptr<Container>> containers;

    for (auto output: this->outputs) {
        //Stup
        containers.push_back(std::make_shared<RIFFContainer>(output));
    }
    return containers;
}


void ConversionManager::convert()
{
    if (this->outputs.empty()) {
        //Nothing to do.
        return;
    }

    auto result = this->checkMapping();
    if (!result.empty()) {
        throw new MappingException(
            "Mapping error",
            result
            );
    }

    std::shared_ptr<Resource> output = nullptr;
    try {
        output = this->outputs.at(0);
    } catch (std::out_of_range& e) {
        //No first output: return.
        return;
    }

    std::vector<std::shared_ptr<Container>> input_containers;

    //Read all input containers:
    for (auto input: this->inputs) {
        //We have opened the file: probe multimedia containers
        auto input_container = this->probeResource(input);
        if (input_container == nullptr) {
            throw Exception(
                fmt::format(
                    "Resource {0}: Unknown input container!",
                    input->toString()
                    )
                );
        }
        input_containers.push_back(input_container);
        input_container->readHeader();
    }
    //Generate all output containers (one for now):
    std::vector<std::shared_ptr<Container>> output_containers = {
        std::make_shared<RIFFContainer>(output)
    };

    std::map<StreamID, std::shared_ptr<AudioCodec>> audio_chains;
    std::map<StreamID, std::shared_ptr<VideoCodec>> video_chains;

    //Map streams, build audio and video chains:
    for (auto mapping_rule: this->mapping) {
        auto input_stream_id = mapping_rule.first;
        auto output_stream_id = mapping_rule.second;
        std::shared_ptr<Container> input_container = nullptr;
        std::shared_ptr<Container> output_container = nullptr;
        try {
            input_container = input_containers.at(input_stream_id.resource_id);
            output_container = output_containers.at(output_stream_id.resource_id);
        } catch (std::out_of_range& e) {
            throw Exception(
                fmt::format(
                    "Error mapping stream {0} to stream {1}!",
                    input_stream_id.toString(),
                    output_stream_id.toString()
                    )
                );
        }
        //Build the conversion chain:
        if (input_stream_id.type != output_stream_id.type) {
            //We have to catch one type of conversion that may
            //be supported in a later version.
            throw Exception(
                fmt::format(
                    "Converting different stream types isn\'t suppoted yet! (Conversion from stream {0} to stream {1})",
                    input_stream_id.toString(),
                    output_stream_id.toString()
                    )
                );
        }
        if (input_stream_id.type == StreamType::AUDIO) {
            auto audio_codec_found = audio_chains.find(input_stream_id);
            std::shared_ptr<AudioCodec> input_audio_codec = nullptr;
            if (audio_codec_found != audio_chains.end()) {
                input_audio_codec = audio_codec_found->second;
            } else {
                auto codec_name = input_container->getAudioCodec(
                    input_stream_id.stream_id
                    );
                if (codec_name == "PCM") {
                    input_audio_codec = std::make_shared<PCMCodec>(
                        input_container->getAudioStreamConfig(
                            input_stream_id.stream_id
                            )
                        );
                }
            }
            if (input_audio_codec == nullptr) {
                throw Exception(
                    fmt::format(
                        "The input audio stream {0} does not exist!",
                        input_stream_id.toString()
                        )
                    );
            }
            input_audio_codec->setContainer(input_container);
            //stub for now: Always output PCM audio.
            auto output_audio_codec = std::make_shared<PPAVC::PCMCodec>();
            if (output_audio_codec == nullptr) {
                throw Exception(
                    fmt::format(
                        "Error creating output codec for stream {0}!",
                        output_stream_id.toString()
                        )
                    );
            }
            output_audio_codec->setEncodeMode();
            output_audio_codec->setContainer(output_container);
            output_container->setAudioCodec(output_stream_id.stream_id, output_audio_codec->getName());
            output_container->setAudioStreamConfig(
                output_stream_id.stream_id,
                input_container->getAudioStreamConfig(input_stream_id.stream_id)
                );

            //Chain the input to the output:
            input_audio_codec->addNextChainObject(output_audio_codec);

            if (audio_codec_found == audio_chains.end()) {
                //The input audio codec hasn't been placed in the audio chains
                //yet. We must do that now.
                audio_chains.insert({input_stream_id, input_audio_codec});
            }
        }
    }

    //Loop over all audio (and later also video) chains and let them read data:
    for (auto chain: audio_chains) {
        auto stream_id = chain.first;
        auto codec = chain.second;

        //Very simple: Let the codec read data until it cannot read any more:
        while(codec->step(262144));  //256k for now
    }
}
