/*
    ConversionManager.cpp
    This file is part of endemm - encoder and decoder for multimedia.
    Copyright (C) 2016 - 2023 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "ConversionManager.h"


using namespace Endemm;


//Protected methods:


uint32_t ConversionManager::bitrateToInt(const std::string& bitrate) noexcept
{
    //Check if the bitrate matches the regex for bitrates:
    std::regex bitrate_regex("([1-9][0-9]*)([km])?");
    std::smatch parts;
    if (!std::regex_match(bitrate, parts, bitrate_regex)) {
        return 0;
    }
    if (parts.size() != 3) {
        return 0;
    }
    uint32_t result = stoul(parts[1].str());
    const std::string multiplier = parts[2].str();
    if (multiplier == "k") {
        result *= 1000;
    } else if (multiplier == "m") {
        result *= 1000000;
    }
    return result;
}


void ConversionManager::printJsonMessage(
    const std::string& message_type,
    const std::map<std::string, std::string>& data
    ) const
{
    std::string output = "{\"msg_type\": \"" + message_type + "\", \"content\": {";
    bool first_attr = true;
    for (auto attribute: data) {
        if (first_attr) {
            first_attr = false;
        } else {
            output += ", ";
        }
        output += "\"" + attribute.first + "\": \"" + attribute.second + "\"";
    }
    output += "}}\n";
    std::cerr << output;
}


void ConversionManager::printConversion(
    std::map<StreamID, std::shared_ptr<Codec>> chains
    )
{
    if (this->text_output_disabled) {
        //Nothing to do.
        return;
    }
    for (auto chain: chains) {
        auto stream_id = chain.first;
        auto input_codec = chain.second;
        if (this->use_json_output) {
            this->printJsonMessage(
                "conversion",
                {
                    {"stream", stream_id.toString()},
                    {"codec", input_codec->getName()}
                }
                );
        } else {
            std::cerr << fmt::format("Stream {0}: Codec {1}\n", stream_id.toString(), input_codec->getName());
        }
    }
}


std::shared_ptr<Resource> ConversionManager::getResourceFromURI(std::string uri, bool write_mode)
{
    //Check if the URI is a real URI or just a file name in the file system:
    auto uri_parts = StringHelper::explode("://", uri, 1);
    std::shared_ptr<Resource> resource = nullptr;
    if (uri_parts.size() == 1) {
        //A file URI.
        resource = std::make_shared<FileResource>(uri, write_mode);
        if (resource != nullptr) {
            auto resource_prober = std::make_shared<SimpleResourceProber>();
            if (resource_prober != nullptr) {
                resource->setProber(resource_prober);
            }
        }
    } else if(uri_parts.size() == 2) {
        //Check the URI scheme to load the correct resource.
        if (uri_parts[0] == "dvd") {
            //It is a DVD resource. The second part of the URI is the
            //position on the DVD (title, chapter or sector).
            resource = std::make_shared<DVDResource>(uri_parts[1]);
            //TODO: add a DVD resource prober.
        }
    }
    return resource;
}


//Public methods:


ConversionManager::ConversionManager()
{
    //Nothing at the moment.
}


ConversionManager::ConversionManager(std::string input_uri, std::string output_uri)
{
    if (input_uri.empty()) {
        //Invalid URI.
        throw Exception("The input URI is invalid.");
    }

    auto input = this->getResourceFromURI(input_uri);
    if (input == nullptr) {
        throw Exception("Error initialising input resource module!");
    }

    //If the input resource is valid, we can push it into the input list:
    this->inputs.insert({"0", input});

    if (!output_uri.empty()) {
        auto output = this->getResourceFromURI(output_uri);
        if (output == nullptr) {
            throw Exception("Error initialising output resource module!");
        }
        //If the output resource is valid, we can push it into the output list:
        this->outputs.insert({"0", output});
    }
}


ConversionManager::~ConversionManager()
{
    //Nothing at the moment.
}


std::shared_ptr<Resource> ConversionManager::getInputResource(const std::string& input_id)
{
    try {
        return this->inputs.at(input_id);
    } catch (std::out_of_range& e) {
        return nullptr;
    }
}


std::shared_ptr<Resource> ConversionManager::getOutputResource(const std::string& output_id)
{
    try {
        return this->outputs.at(output_id);
    } catch (std::out_of_range& e) {
        return nullptr;
    }
}


void ConversionManager::addInput(std::string input, std::string alias)
{
    if (input.empty()) {
        //Nothing to do.
        return;
    }

    try {
        auto input_resource = this->getResourceFromURI(input);
        if (input_resource != nullptr) {
            if (alias.empty()) {
                this->inputs.insert(
                    {std::to_string(this->inputs.size()), input_resource}
                    );
            } else {
                this->inputs.insert({alias, input_resource});
            }
        }
    } catch (Exception& e) {
        //TODO: error handling.
        return;
    }
}


void ConversionManager::addOutput(std::string output, std::string alias)
{
    if (output.empty()) {
        //Nothing to do.
        return;
    }

    try {
        auto output_resource = this->getResourceFromURI(output, true);
        if (output_resource != nullptr) {
            if (alias.empty()) {
                this->outputs.insert(
                    {std::to_string(this->outputs.size()), output_resource}
                    );
            } else {
                this->inputs.insert({alias, output_resource});
            }
        }
    } catch (Exception& e) {
        //TODO: error handling.
        return;
    }
}



void ConversionManager::addOutputResource(std::shared_ptr<Resource> resource)
{
    if (resource != nullptr) {
        this->outputs.insert({std::to_string(this->outputs.size()), resource});
    }
}


void ConversionManager::setCodec(const std::string& codec_name, const std::string& stream)
{
    if (codec_name.empty() || stream.empty()) {
        //Nothing to do.
        return;
    }
    if (stream == "*:A*") {
        //Set the global audio codec:
        this->global_audio_config.codec_name = codec_name;
    } else if (stream == "*:V*") {
        //Set the global video codec:
        this->global_video_config.codec_name = codec_name;
    } else {
        //Set the codec for one specific stream:
        StreamID stream_id(stream);
        //Check if there is already a configuration for the stream:
        auto stream_config_it = this->stream_codec_configs.find(stream_id);
        if (stream_config_it == this->stream_codec_configs.end()) {
            //Insert a new stream configuration:
            CodecConfig config;
            config.codec_name = codec_name;
            this->stream_codec_configs.insert({stream_id, config});
        } else {
            //Update the existing configuration:
            stream_config_it->second.codec_name = codec_name;
        }
    }
}


void ConversionManager::setBitrate(const std::string& bitrate_str, const std::string& stream)
{
    if (bitrate_str.empty() || stream.empty()) {
        //Nothing to do.
        return;
    }
    auto bitrate = this->bitrateToInt(bitrate_str);
    if (bitrate == 0) {
        throw Exception(fmt::format("Invalid bitrate: \"{0}\"", bitrate_str));
    }
    if (stream == "*:A*") {
        //Set the global audio bitare:
        this->global_audio_config.bitrate_or_quality = bitrate;
        this->global_audio_config.uses_quality = false;
    } else if (stream == "*:V*") {
        //Set the global video bitrate:
        this->global_video_config.bitrate_or_quality = bitrate;
        this->global_video_config.uses_quality = false;
    } else {
        //Set the bitrate for one specific stream:
        StreamID stream_id(stream);
        //Check if there is already a configuration for the stream:
        auto stream_config_it = this->stream_codec_configs.find(stream_id);
        if (stream_config_it == this->stream_codec_configs.end()) {
            //Insert a new stream configuration:
            CodecConfig config;
            config.bitrate_or_quality = bitrate;
            config.uses_quality = false;
            this->stream_codec_configs.insert({stream_id, config});
        } else {
            //Update the existing configuration:
            stream_config_it->second.bitrate_or_quality = bitrate;
            stream_config_it->second.uses_quality = false;
        }
    }
}


void ConversionManager::setQuality(const std::string& quality_str, const std::string& stream)
{
    if (quality_str.empty() || stream.empty()) {
        //Nothing to do.
        return;
    }
    uint32_t quality = 0;
    try {
        quality = std::stoul(quality_str);
    } catch (std::invalid_argument& e) {
        throw Exception(fmt::format("Invalid quality value: \"{0}\"", quality_str));
    } catch (std::out_of_range& e) {
        throw Exception(fmt::format("Invalid quality value: \"{0}\"", quality_str));
    }
    if (stream == "*:A*") {
        //Set the global audio quality:
        this->global_audio_config.bitrate_or_quality = quality;
        this->global_audio_config.uses_quality = true;
    } else if (stream == "*:V*") {
        //Set the global video quality:
        this->global_video_config.bitrate_or_quality = quality;
        this->global_video_config.uses_quality = true;
    } else {
        //Set the quality for one specific stream:
        StreamID stream_id(stream);
        //Check if there is already a configuration for the stream:
        auto stream_config_it = this->stream_codec_configs.find(stream_id);
        if (stream_config_it == this->stream_codec_configs.end()) {
            //Insert a new stream configuration:
            CodecConfig config;
            config.bitrate_or_quality = quality;
            config.uses_quality = true;
            this->stream_codec_configs.insert({stream_id, config});
        } else {
            //Update the existing configuration:
            stream_config_it->second.bitrate_or_quality = quality;
            stream_config_it->second.uses_quality = true;
        }
    }
}


void ConversionManager::enableJsonOutput()
{
    this->use_json_output = true;
}


void ConversionManager::disableTextOutput()
{
    this->text_output_disabled = true;
}


std::shared_ptr<AudioCodec> ConversionManager::createAudioCodec(
    const std::string& codec_name,
    const AudioStreamConfig& config
    )
{
    std::shared_ptr<AudioCodec> codec = nullptr;
    if (codec_name == "acopy") {
        codec = std::make_shared<AudioCodec>(config);
    } else if (codec_name == "pcm") {
        codec = std::make_shared<PCMCodec>(config);
    } else {
        throw Exception(fmt::format("Unknown codec: {0}", codec_name));
    }
    return codec;
}


std::shared_ptr<VideoCodec> ConversionManager::createVideoCodec(
    const std::string& codec_name,
    const VideoStreamConfig& config
    )
{
    std::shared_ptr<VideoCodec> codec = nullptr;
    if (codec_name == "vcopy") {
        codec = std::make_shared<VideoCodec>(config);
    } else {
        throw Exception(fmt::format("Unknown codec: {0}", codec_name));
    }
    return codec;
}


void ConversionManager::setMapping(
    std::vector<std::pair<StreamID, StreamID>> mapping,
    bool use_sequential_mapping
    )
{
    this->mapping = mapping;
    this->use_sequential_mapping = use_sequential_mapping;
}


std::vector<std::pair<StreamID, std::string>> ConversionManager::checkMapping()
{
    std::vector<std::pair<StreamID, std::string>> result;
    for (auto rule: this->mapping) {
        auto input_stream_id = rule.first;
        auto output_stream_id = rule.second;
        if (this->inputs.find(input_stream_id.resource_id) == this->inputs.end()) {
            //Invalid input resource ID.
            result.push_back(
                std::pair<StreamID, std::string>(
                    input_stream_id,
                    fmt::format(
                        "Stream {0}: No input resource available with the ID {1}.",
                        input_stream_id.toString(),
                        input_stream_id.resource_id
                        )
                    )
                );
        }
        if (this->outputs.find(output_stream_id.resource_id) == this->outputs.end()) {
            //Invalid output resource ID.
            result.push_back(
                std::pair<StreamID, std::string>(
                    output_stream_id,
                    fmt::format(
                        "In Stream {0}: No output resource available with the ID {1}.",
                        output_stream_id.toString(),
                        output_stream_id.resource_id
                        )
                    )
                );
        }
    }
    return result;
}


std::vector<std::shared_ptr<Container>> ConversionManager::createOutputContainers()
{
    std::vector<std::shared_ptr<Container>> containers;

    for (auto output: this->outputs) {
        //Stup
        containers.push_back(std::make_shared<RIFFContainer>(output.second));
    }
    return containers;
}


void ConversionManager::convert()
{
    if (this->outputs.empty()) {
        //Nothing to do.
        return;
    }

    auto result = this->checkMapping();
    if (!result.empty()) {
        throw new MappingException(
            "Mapping error",
            result
            );
    }

    std::map<std::string, std::shared_ptr<Container>> input_containers;

    //Read all input containers:
    for (auto input: this->inputs) {
        //We have opened the file: probe multimedia containers
        auto input_container = input.second->probe();
        if (input_container == nullptr) {
            throw Exception(
                fmt::format(
                    "Resource {0}: Unknown input container!",
                    input.second->toString()
                    )
                );
        }
        input_containers.insert({input.first, input_container});
        input_container->readHeader();
    }
    //Generate all output containers:
    std::map<std::string, std::shared_ptr<Container>> output_containers;
    for (auto output: this->outputs) {
        output_containers.insert(
            {output.first, std::make_shared<RIFFContainer>(output.second)}
            );
    };

    //If no mapping rules are set, generate them:
    //Map each input stream to every output.
    if (this->mapping.empty()) {
        for (auto input_map: input_containers) {
            auto input_id = input_map.first;
            auto input_container = input_map.second;
            if (input_container == nullptr) {
                continue;
            }
            auto input_audio_streams = input_container->getAudioStreams();
            auto input_video_streams = input_container->getVideoStreams();

            for (auto output_map: output_containers) {
                auto output_id = output_map.first;
                auto output_container = output_map.second;
                if (output_container == nullptr) {
                    continue;
                }
                for (auto input_stream: input_audio_streams) {
                    auto input_stream_number = input_stream.first;
                    auto codec_name = input_stream.second;
                    StreamID input_stream_id(input_id, input_stream_number, StreamType::AUDIO);
                    auto output_stream_number = output_container->addAudioStream(codec_name);
                    StreamID output_stream_id(output_id, output_stream_number, StreamType::AUDIO);
                    this->mapping.push_back({input_stream_id, output_stream_id});
                }
                for (auto input_stream: input_video_streams) {
                    auto input_stream_number = input_stream.first;
                    auto codec_name = input_stream.second;
                    StreamID input_stream_id(input_id, input_stream_number, StreamType::VIDEO);
                    auto output_stream_number = output_container->addVideoStream(codec_name);
                    StreamID output_stream_id(output_id, output_stream_number, StreamType::VIDEO);
                    this->mapping.push_back({input_stream_id, output_stream_id});
                }
            }
        }
    }

    std::map<StreamID, std::shared_ptr<Codec>> chains;

    //Map streams, build audio and video chains:
    for (auto mapping_rule: this->mapping) {
        auto input_stream_id = mapping_rule.first;
        auto output_stream_id = mapping_rule.second;
        auto input_container_it = input_containers.find(input_stream_id.resource_id);
        auto output_container_it = output_containers.find(output_stream_id.resource_id);
        if (input_container_it == input_containers.end()
            || output_container_it == output_containers.end()) {
            throw Exception(
                fmt::format(
                    "Error mapping stream {0} to stream {1}!",
                    input_stream_id.toString(),
                    output_stream_id.toString()
                    )
                );
        }
        auto input_container = input_container_it->second;
        auto output_container = output_container_it->second;
        if ((input_container == nullptr) || (output_container == nullptr)) {
            throw Exception(
                fmt::format(
                    "Error mapping stream {0} to stream {1}!",
                    input_stream_id.toString(),
                    output_stream_id.toString()
                    )
                );
        }
        //Build the conversion chain:
        if (input_stream_id.type != output_stream_id.type) {
            //We have to catch one type of conversion that may
            //be supported in a later version.
            throw Exception(
                fmt::format(
                    "Converting different stream types isn\'t suppoted yet! (Conversion from stream {0} to stream {1})",
                    input_stream_id.toString(),
                    output_stream_id.toString()
                    )
                );
        }
        auto codec_found = chains.find(input_stream_id);
        std::shared_ptr<Codec> input_codec = nullptr;
        if (codec_found != chains.end()) {
            input_codec = codec_found->second;
        } else {
            if (input_stream_id.type == StreamType::AUDIO) {
                auto codec_name = input_container->getAudioCodec(
                    input_stream_id.stream_id
                    );
                input_codec = this->createAudioCodec(
                    codec_name,
                    input_container->getAudioStreamConfig(
                        input_stream_id.stream_id
                        )
                    );
            } else if (input_stream_id.type == StreamType::VIDEO) {
                auto codec_name = input_container->getVideoCodec(
                    input_stream_id.stream_id
                    );
                input_codec = this->createVideoCodec(
                    codec_name,
                    input_container->getVideoStreamConfig(
                        input_stream_id.stream_id
                        )
                    );
            }
        }
        if (input_codec == nullptr) {
            throw Exception(
                fmt::format(
                    "The input stream {0} does not exist!",
                    input_stream_id.toString()
                    )
                );
        }
        input_codec->setContainer(input_container);
        CodecConfig output_codec_config;
        auto stream_codec_config_it = this->stream_codec_configs.find(output_stream_id);
        if (stream_codec_config_it != this->stream_codec_configs.end()) {
            //There is a stream-specific codec configuration.
            output_codec_config = stream_codec_config_it->second;
        } else {
            //Use the global codec configuration.
            if (!this->global_audio_config) {
                throw Exception("No output codec is specified.");
            }
            output_codec_config = this->global_audio_config;
        }
        //Create an output codec with the same configuration
        //as the input.
        std::shared_ptr<Codec> output_codec = nullptr;
        if (input_stream_id.type == StreamType::AUDIO) {
            output_codec = this->createAudioCodec(
                output_codec_config.codec_name,
                input_container->getAudioStreamConfig(input_stream_id.stream_id)
                );
        } else if (input_stream_id.type == StreamType::VIDEO) {
            output_codec = this->createVideoCodec(
                output_codec_config.codec_name,
                input_container->getVideoStreamConfig(input_stream_id.stream_id)
                );
        }
        if (output_codec == nullptr) {
            throw Exception(
                fmt::format(
                    "Error creating output codec for stream {0}!",
                    output_stream_id.toString()
                    )
                );
        }
        output_codec->setEncodeMode();
        output_codec->setContainer(output_container);
        if (output_codec_config.uses_quality) {
            output_codec->setQuality(output_codec_config.bitrate_or_quality);
        } else {
            output_codec->setBitrate(output_codec_config.bitrate_or_quality);
        }
        if (input_stream_id.type == StreamType::AUDIO) {
            output_container->setAudioCodec(output_stream_id.stream_id, output_codec->getName());
            output_container->setAudioStreamConfig(
                output_stream_id.stream_id,
                input_container->getAudioStreamConfig(input_stream_id.stream_id)
                );
            //Chain the input to the output:
            std::shared_ptr<AudioCodec> input_audio_codec = std::dynamic_pointer_cast<AudioCodec>(input_codec);
            std::shared_ptr<AudioCodec> output_audio_codec = std::dynamic_pointer_cast<AudioCodec>(output_codec);
            input_audio_codec->addNextChainObject(output_audio_codec);
        } else if (input_stream_id.type == StreamType::VIDEO) {
            output_container->setVideoCodec(output_stream_id.stream_id, output_codec->getName());
            output_container->setVideoStreamConfig(
                output_stream_id.stream_id,
                input_container->getVideoStreamConfig(input_stream_id.stream_id)
                );
            //Chain the input to the output:
            std::shared_ptr<VideoCodec> input_video_codec = std::dynamic_pointer_cast<VideoCodec>(input_codec);
            std::shared_ptr<VideoCodec> output_video_codec = std::dynamic_pointer_cast<VideoCodec>(output_codec);
            input_video_codec->addNextChainObject(output_video_codec);
        }

        if (codec_found == chains.end()) {
            //The input codec hasn't been placed in the chain yet.
            //We must do that now.
            chains.insert({input_stream_id, input_codec});
        }
        if (!this->text_output_disabled) {
            if (this->use_json_output) {
                std::map<std::string, std::string> json_data = {
                    {"input_stream_id", input_stream_id.toString()},
                    {"output_stream_id", output_stream_id.toString()},
                    {"input_codec", input_codec->getName()},
                    {"output_codec", output_codec->getName()},
                };
                if (output_codec_config.uses_quality) {
                    json_data["output_quality"] = std::to_string(output_codec_config.bitrate_or_quality);
                } else {
                    json_data["output_bitrate"] = std::to_string(output_codec_config.bitrate_or_quality);
                }
                this->printJsonMessage("stream_mapping", json_data);
            } else {
                std::cerr << fmt::format(
                    "{0} -> {1}\t{2} -> {3} ({4}: {5})",
                    input_stream_id.toString(),
                    output_stream_id.toString(),
                    input_codec->getName(),
                    output_codec->getName(),
                    (output_codec_config.uses_quality ? "Quality" : "Bitrate"),
                    output_codec_config.bitrate_or_quality
                    ) << "\n";
            }
        }
    }

    //Output the conversion data:

    this->printConversion(chains);

    //Loop over all audio (and later also video) chains and let them read data:
    for (auto chain: chains) {
        auto stream_id = chain.first;
        auto codec = chain.second;

        //Very simple: Let the codec read data until it cannot read any more:
        while(codec->step(262144));  //256k for now
    }
}
