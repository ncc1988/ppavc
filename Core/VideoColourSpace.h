/*
    VideoColourSpace.h
    This file is part of ppavc - planned program for audio and video conversion.
    Copyright (C) 2016 - 2022 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef PPAVC__VIDEOCOLOURSPACE_H
#define PPAVC__VIDEOCOLOURSPACE_H


namespace PPAVC
{
    /**
     * A class specifying the colour spaces in which a video codec can accept
     * input data for encoding or in which it can output data after decoding.
     */
    enum class VideoColourSpace
    {
        /**
         * RGB, 24 bit (8 bit per channel).
         */
        RGB24,

        /**
         * RGB and alpha, 32 bit (8 bit per channel).
         */
        RGBA32,

        /**
         * 16 bit RGB
         */
        RGB565,

        /**
         * RGB, 48 bit (16 bit per channel).
         */
        RGB48,

        /**
         * RGB and alpha, 64 bit (16 bit per channel).
         */
        RGBA64

        //TODO: add YUV-based formats.
    };
}


#endif
