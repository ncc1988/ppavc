/*
    CodecConfig.h
    This file is part of endemm - encoder and decoder for multimedia.
    Copyright (C) 2016 - 2023 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef ENDEMM__CODECCONFIG_H
#define ENDEMM__CODECCONFIG_H


namespace Endemm
{
    class CodecConfig
    {
        public:


        /**
         * The bitrate in bits per second or the quality
         * in percent (0 - 100).
         */
        uint32_t bitrate_or_quality;


        /**
         * True, if bitrate_or_quality contains a quality value.
         * False, if bitrate_or_quality contains a bitrate value.
         */
        bool uses_quality;


        /**
         * The name of the used codec.
         */
        std::string codec_name;

        /**
         * The not operator can be used to determine whether the CodecConfig
         * object is empty/uninitialised (true) or not (false).
         */
        bool operator!()
        {
            return this->bitrate_or_quality == 0 && this->codec_name.empty();
        }
    };
}


#endif
