/*
    StreamID.h
    This file is part of endemm - encoder and decoder for multimedia.
    Copyright (C) 2016 - 2024 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef ENDEMM__STREAMID_H
#define ENDEMM__STREAMID_H


#include <sstream>
#include <string>
#include <vector>


#include <fmt/core.h>


#include "StringConvertible.h"


namespace Endemm
{

    /**
     * An enum class for specifying stream types.
     */
    enum class StreamType
    {
        NONE,
        AUDIO,
        VIDEO,
        TEXT
    };


    /**
     * A class for building an unique ID for media streams.
     */
    class StreamID : StringConvertible
    {
        public:


        /**
         * The resource ID for the stream. This can be a number or a name.
         */
        std::string resource_id = "";


        /**
         * The stream number of the stream.
         */
        uint8_t stream_id = 0;


        /**
         * The type of the stream.
         */
        StreamType type = StreamType::NONE;


        StreamID(const std::string& resource_id, uint8_t stream_id, StreamType type)
            : resource_id(resource_id),
              stream_id(stream_id),
              type(type)
        {
            //Nothing else
        }


        StreamID(const std::string& stream_str)
        {
            //Split the string by the ':' character:
            std::vector<std::string> parts;
            std::stringstream stream_str_content(stream_str);
            std::string part;
            while (std::getline(stream_str_content, part, ':')) {
                parts.push_back(part);
            }

            if (parts.size() == 2) {
                //parts[0] is the resource_id, while
                //parts[1] starts with A or V and the stream id.
                if (!parts[0].empty()) {
                    std::string stream_part = parts[1];
                    if (stream_part.length() >= 2) {
                        bool valid_stream_str = true;
                        auto stream_id_str = stream_part.substr(1);
                        uint32_t stream_id_value = 0;
                        try {
                            stream_id_value = stoul(stream_id_str);
                        } catch (std::invalid_argument& e) {
                            valid_stream_str = false;
                        } catch (std::out_of_range& e) {
                            valid_stream_str = false;
                        }
                        if (stream_id_value > 255) {
                            valid_stream_str = false;
                        }
                        if (stream_part[0] == 'A') {
                            this->type = StreamType::AUDIO;
                        } else if (stream_part[0] == 'V') {
                            this->type = StreamType::VIDEO;
                        } else {
                            valid_stream_str = false;
                        }
                        if (valid_stream_str) {
                            this->resource_id = parts[0];
                            this->stream_id = stream_id_value;
                        }
                    }
                }
            }
        }


        bool empty() const
        {
            return this->resource_id.empty() && (this->stream_id == 0)
                && (this->type == StreamType::NONE);
        }


        /**
         * Checks whether two StreamID objects are equal.
         *
         * @returns True if both StreamID objects are equal, false otherwise.
         */
        bool operator==(const StreamID& other) const
        {
            return (this->resource_id == other.resource_id)
                && (this->stream_id == other.stream_id)
                && (this->type == other.type);
        }


        bool operator<(const StreamID& other) const
        {
            if (*this == other) {
                return false;
            }
            if (this->resource_id > other.resource_id) {
                return false;
            }
            if (this->stream_id > other.stream_id) {
                return false;
            }
            if (this->type > other.type) {
                return false;
            }
            return true;
        }


        /**
         * @see StringConvertible::toString
         */
        virtual std::string toString()
        {
            return fmt::format(
                "{0}:{1}{2}",
                this->resource_id,
                (
                    this->type == StreamType::AUDIO
                    ? "A"
                    : (
                        this->type == StreamType::VIDEO
                        ? "V"
                        : (
                            this->type == StreamType::TEXT
                            ? "T"
                            : ""
                            )
                        )
                    ),
                this->stream_id
                );
        }
    };
}


#endif
