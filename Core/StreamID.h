/*
    StreamID.h
    This file is part of ppavc - planned program for audio and video conversion.
    Copyright (C) 2016 - 2022 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef PPAVC__STREAMID_H
#define PPAVC__STREAMID_H


#include <string>


#include <fmt/core.h>


#include "StringConvertible.h"


namespace PPAVC
{

    /**
     * An enum class for specifying stream types.
     */
    enum class StreamType
    {
        AUDIO,
        VIDEO,
        TEXT
    };


    /**
     * A class for building an unique ID for media streams.
     */
    class StreamID : StringConvertible
    {
        public:


        /**
         * The resource number for the stream.
         */
        uint8_t resource_id;


        /**
         * The stream number of the stream.
         */
        uint8_t stream_id;


        /**
         * The type of the stream.
         */
        StreamType type;


        StreamID(uint8_t resource_id, uint8_t stream_id, StreamType type)
            : resource_id(resource_id),
              stream_id(stream_id),
              type(type)
        {
            //Nothing else
        }


        /**
         * Checks whether two StreamID objects are equal.
         *
         * @returns True if both StreamID objects are equal, false otherwise.
         */
        bool operator==(const StreamID& other) const
        {
            return (this->resource_id == other.resource_id)
                && (this->stream_id == other.stream_id)
                && (this->type == other.type);
        }


        bool operator<(const StreamID& other) const
        {
            if (*this == other) {
                return false;
            }
            if (this->resource_id > other.resource_id) {
                return false;
            }
            if (this->stream_id > other.stream_id) {
                return false;
            }
            if (this->type > other.type) {
                return false;
            }
            return true;
        }


        /**
         * @see StringConvertible::toString
         */
        virtual std::string toString()
        {
            return fmt::format(
                "{0}:{1}{2}",
                this->resource_id,
                (
                    this->type == StreamType::AUDIO
                    ? "A"
                    : (
                        this->type == StreamType::VIDEO
                        ? "V"
                        : (
                            this->type == StreamType::TEXT
                            ? "T"
                            : ""
                            )
                        )
                    ),
                this->stream_id
                );
        }
    };
}


#endif
