/*
    Resource.h
    This file is part of endemm - encoder and decoder for multimedia.
    Copyright (C) 2016 - 2024 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef ENDEMM__RESOURCE_H
#define ENDEMM__RESOURCE_H


#include <memory>
#include <string>
#include <vector>


#include "../Containers/Container.h"
#include "ResourceProber.h"
#include "StringConvertible.h"


namespace Endemm
{
    //Due to circular inclusion, the Container an ResourceProber classes
    //are declared here.
    class Container;
    class ResourceProber;


    /**
     * A resource is a data source or sink. It is used by containers
     * to read data from or to write data to.
     *
     * Implementations of the Resource class could handle file-based
     * reading/writing or reading/writing data via sound cards,
     * posix sockets, network connections or other input/output resources.
     */
    class Resource: public StringConvertible, public std::enable_shared_from_this<Resource>
    {
        protected:


        /**
         * The ID of the resource. It should be unique during runtime.
         */
        uint16_t id = 0;


        /**
         * A location specifier for the resuource: The file name, the URL, etc.
         */
        std::string location = "";


        /**
         * Whether reading from the resource is possible.
         */
        bool read_access = false;


        /**
         * Whether writing to the resource is possible.
         */
        bool write_access = false;


        /**
         * The prober to be used for probing this resource.
         */
        std::shared_ptr<ResourceProber> prober = nullptr;


        public:


        /**
         * Initialises a Resource instance and specifies its location.
         *
         * @param const std::string& location The location of the resource.
         *     The content of this string is implementation-dependent.
         */
        Resource(const std::string& location = "");


        /**
         * @returns uint16_t The ID of the resource.
         */
        virtual uint16_t getId();


        /**
         * Sets the ID of the resource.
         *
         * @param uint16_t id The ID of the resource.
         */
        virtual void setId(uint16_t id);


        /**
         * @returns std::string The location of the resource.
         */
        virtual std::string getLocation();


        /**
         * Sets the location of the resource.
         *
         * @param const std::string& location The location of the resource.
         */
        virtual void setLocation(const std::string& location);


        /**
         * Returns the state of the read_access flag.
         *
         * @returns bool True, if read access on the resource is possible,
         *     false otherwise.
         */
        virtual bool isReadable();


        /**
         * Returns the state of the write_access flag.
         *
         * @returns bool True, if write access on the resource is possible,
         *     false otherwise.
         */
        virtual bool isWritable();


        /**
         * Sets the prober that shall be used to probe the resource.
         */
        virtual void setProber(std::shared_ptr<ResourceProber> prober);

        /**
         * Returns the resource prober for this resource.
         *
         * @returns std::shared_ptr<ResourceProber> The resource prober for the
         *     resource or a nullptr if no prober is set.
         */
        virtual std::shared_ptr<ResourceProber> getProber();

        /**
         * This method probes the resource to find the correct container for the
         * content. If no matching container can be found, null is returned.
         * The probing is done with a resource prober instance.
         *
         * @returns std::shared_ptr<Container> The matching container for the
         *     resource or a nullptr if no such container can be found.
         */
        virtual std::shared_ptr<Container> probe();


        /**
         * Writes an object or data structure to the resource.
         *
         * @param const uint8_t* data The object to be written.
         *
         * @param size_t size The size of data.
         *
         * @returns bool True, if the object could be written, false otherwise.
         */
        virtual bool writeObject(const uint8_t* data, size_t size) = 0;


        /**
         * Writes an object or data structure to a specific position
         * of the resource.
         *
         * @param const uint8_t* data The object to be written.
         *
         * @param size_t size The size of data.
         *
         * @param size_t position The position where data shall be written to.
         *
         * @returns bool True, if the object could be written, false otherwise.
         */
        virtual bool writeObject(const uint8_t* data, size_t size, size_t position) = 0;


        /**
         * Reads an object or data structure from the resource.
         *
         * @param const uint8_t* data The object to be read.
         *
         * @param size_t size The size of data.
         *
         * @returns bool True, if the object could be read, false otherwise.
         */
        virtual bool readObject(const uint8_t* data, size_t size) = 0;


        /**
         * Reads an object or data structure from a specific position
         * of the resource.
         *
         * @param const uint8_t* data The object to be read.
         *
         * @param size_t size The size of data.
         *
         * @param size_t position The position where data shall be read from.
         *
         * @returns bool True, if the object could be read, false otherwise.
         */
        virtual bool readObject(const uint8_t* data, size_t size, size_t position) = 0;


        /**
         * Writes data to the resource.
         *
         * @param const std::vector<uint8_t>& data The data to be written.
         *
         * @returns size_t The amount of bytes that have been written.
         */
        virtual size_t write(const std::vector<uint8_t>& data) = 0;


        /**
         * Writes data to a specified position of the resource.
         *
         * @param const std::vector<uint8_t>& data The data to be written.
         *
         * @param size_t position The position where data shall be written to.
         *
         * @returns size_t The amount of bytes that have been written.
         */
        virtual size_t write(const std::vector<uint8_t>& data, size_t position) = 0;


        /**
         * Reads data from the resource.
         *
         * @param size_t size The size of the data.
         *
         * @returns std::vector<uint8_t> The data that have been read.
         */
        virtual std::vector<uint8_t> read(size_t size) = 0;


        /**
         * Reads data from a specified position of the resource.
         *
         * @param size_t size The size of the data.
         *
         * @param size_t position The position from which data shall be read.
         *
         * @returns std::vector<uint8_t> The data that have been read.
         */
        virtual std::vector<uint8_t> read(size_t size, size_t position) = 0;


        /**
         * Seeks to the absolute position in the resource.
         * The unit of measurement for the position is one byte.
         *
         * Note that some resource implementations may not be able to seek
         * at all or only in a small buffered portion of data. Think of this
         * as a volatile stream that is only buffered.
         *
         * @param size_t position The position to seek to.
         *
         * @returns bool True, if seeking was successful, false otherwise.
         */
        virtual bool seekAbsolutePosition(size_t position) = 0;


        /**
         * Seeks forward from the current position.
         * The unit of measurement for the offset is one byte.
         *
         * Note that some resource implementations may not be able to seek
         * at all or only in a small buffered portion of data. Think of this
         * as a volatile stream that is only buffered.
         *
         * @param size_t offset The position to move forward to.
         *
         * @returns bool True, if moving forward was successful,
         *     false otherwise.
         */
        virtual bool moveForward(size_t offset) = 0;


        /**
         * Seeks backwards from the current position.
         * The unit of measurement for the offset is one byte.
         *
         * Note that some resource implementations may not be able to seek
         * at all or only in a small buffered portion of data. Think of this
         * as a volatile stream that is only buffered.
         *
         * @param size_t offset The position to move backwards to.
         *
         * @returns bool True, if moving backwards was successful,
         *     false otherwise.
         */
        virtual bool moveBackward(size_t offset) = 0;


        /**
         * @see StringConvertible::toString
         */
        virtual std::string toString();
    };
}


#endif
