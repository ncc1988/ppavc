/*
    FileResource.cpp
    This file is part of ppavc - planned program for audio and video conversion.
    Copyright (C) 2016 - 2022 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "FileResource.h"


using namespace PPAVC;


FileResource::FileResource(const std::string& location, bool write_mode)
    : Resource(location)
{
    this->read_access = true;
    if (write_mode) {
        this->write_access = true;
    }
}


FileResource::~FileResource()
{
    if (this->file != nullptr) {
        this->file->close();
    }
}



void FileResource::openFile()
{
    if (this->file != nullptr) {
        //File already opened.
        return;
    }

    if (this->write_access) {
        this->file = std::make_unique<std::fstream>(this->location, std::ios::out | std::ios::binary);
    } else {
        this->file = std::make_unique<std::fstream>(this->location, std::ios::in | std::ios::binary);
    }
    if (this->file == nullptr) {
        throw Exception(
            fmt::format(
                "File {0} not found!",
                this->location
                )
            );
    }
    if (!this->file->is_open()) {
        throw Exception(
            fmt::format(
                "Error opening file {0}!",
                this->location
                )
            );
    }
    if (!this->write_access) {
        this->file->seekg(0, std::ios::end);
        this->file_size = this->file->tellg();
        this->file->seekg(0, std::ios::beg);
    }
}



bool FileResource::writeObject(const uint8_t* data, size_t size)
{
    if (!this->write_access) {
        throw Exception("No write access!");
    }
    if (this->file == nullptr) {
        this->openFile();
    }
    this->file->write((char*)data, size);
    if (this->file->fail()) {
        return false;
    }

    return true;
}


bool FileResource::writeObject(const uint8_t* data, size_t size, size_t position)
{
    if (!this->write_access) {
        throw Exception("No write access!");
    }
    if (this->file == nullptr) {
        this->openFile();
    }

    //Seek to the requested position:
    this->file->seekp(position);

    this->file->write((char*)data, size);
    if (this->file->fail()) {
        return false;
    }

    return true;
}


bool FileResource::readObject(const uint8_t* data, size_t size)
{
    if (!this->read_access) {
        throw Exception("No read access!");
    }
    if (this->file == nullptr) {
        this->openFile();
    }
    if ((size_t)(this->file->tellg()) + size > this->file_size) {
        //What should we do with half an object?
        return false;
    }
    this->file->read((char*)data, size);
    if (this->file->fail()) {
        return false;
    }
    return true;
}


bool FileResource::readObject(const uint8_t* data, size_t size, size_t position)
{
    if (!this->read_access) {
        throw Exception("No read access!");
    }
    if (this->file == nullptr) {
        this->openFile();
    }
    if (position + size > this->file_size) {
        //What should we do with half an object?
        return false;
    }

    //Seek to the requested position:
    this->file->seekg(position);

    this->file->read((char*)data, size);
    if (this->file->fail()) {
        return false;
    }
    return true;
}


size_t FileResource::write(const std::vector<uint8_t>& data)
{
    if (!this->write_access) {
        throw Exception("No write access!");
    }
    if (this->file == nullptr) {
        this->openFile();
    }
    this->file->write((char*)data.data(), data.size());
    if (this->file->fail()) {
        return false;
    }

    return data.size();
}


size_t FileResource::write(const std::vector<uint8_t>& data, size_t position)
{
    if (!this->write_access) {
        throw Exception("No write access!");
    }
    if (this->file == nullptr) {
        this->openFile();
    }

    //Seek to the requested position:
    this->file->seekp(position);

    this->file->write((char*)data.data(), data.size());
    if (this->file->fail()) {
        return false;
    }

    return data.size();
}


std::vector<uint8_t> FileResource::read(size_t size)
{
    if (!this->read_access) {
        throw Exception("No read access!");
    }
    if (this->file == nullptr) {
        this->openFile();
    }
    size_t position = this->file->tellg();
    size_t read_size = size;
    if (position + size > this->file_size) {
        read_size = this->file_size - position;
    }
    if (read_size == 0) {
        return {};
    }
    std::vector<uint8_t> data(read_size);
    if (data.size() != read_size) {
        //Not enough space allocated.
        throw Exception("Allocation error!");
    }
    this->file->read((char*)data.data(), read_size);
    if (this->file->fail()) {
        return {};
    }
    return data;
}


std::vector<uint8_t> FileResource::read(size_t size, size_t position)
{
    if (!this->read_access) {
        throw Exception("No read access!");
    }
    if (this->file == nullptr) {
        this->openFile();
    }
    if (position > -1) {
        this->file->clear();
        //Seek to the requested position:
        this->file->seekg(position);
    }
    return this->read(size);
}
