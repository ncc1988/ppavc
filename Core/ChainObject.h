/*
    ChainObject.h
    This file is part of endemm - encoder and decoder for multimedia.
    Copyright (C) 2016 - 2024 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef ENDEMM__CHAINOBJECT_H
#define ENDEMM__CHAINOBJECT_H


#include <memory>
#include <vector>


namespace Endemm
{
    /**
     * The ChainObject class is the base class for chaining audio and video
     * conversion items. Items can be codecs, processors or other kind of
     * items that manipulate audio or video data.
     */
    class ChainObject: public std::enable_shared_from_this<ChainObject>
    {
        protected:

        /**
         * This is a list of pointers to the next objects in the chain.
         */
        std::vector<std::shared_ptr<ChainObject>> next_chain_objects;

        public:

        //Raw data (bytes) processing methods:

        /**
         * This method is used for the last object in an audio chain.
         * That object has no next objects where it could send data,
         * so it must send them somewhere else outside of the chain.
         *
         * @param std::vector<uint8_t> data The data to send to the endpoint.
         *
         * @param bool last_chunk Whether data is the last chunk of data (true)
         *     or not (false).
         */
        virtual void sendDataToEndpoint(std::vector<uint8_t> data, bool last_chunk = false) = 0;

        /**
         * Receives data, processes them and passes them on to the next
         * chain objects. If no next chain object is defined, the method
         * sendDataToEndpoint is called.
         *
         * @param std::vector<uint8_t> data The data to be received.
         *
         * @param bool last_chunk Whether data is the last chunk of data (true)
         *     or not (false).
         */
        virtual void receiveData(std::vector<uint8_t> data, bool last_chunk = false);

        /**
         * Processes data.
         *
         * @param std::vector<uint8_t> input Input data.
         *
         * @returns std::vector<uint8_t> The processed data.
         */
        virtual std::vector<uint8_t> processData(std::vector<uint8_t> input) = 0;

        //Chain-related methods:

        /**
         * Seeks through a chain to find out if the item is in the chain.
         *
         * @param std::shared_ptr<ChainObject> needle The item to search for.
         *
         * @param std::shared_ptr<ChainObject> haystack The chain to be searched.
         *
         * @returns True, if $needle is in $haystack or if $needle == $haystack.
         *     In all other case, false is returned.
         */
        static bool isInChain(
            std::shared_ptr<ChainObject> needle,
            std::shared_ptr<ChainObject> haystack
            );

        /**
         * Adds a chain object to the list of chain objects that follow this
         * chain object.
         *
         * @param std::shared_ptr<ChainObject> next The item to be added.
         */
        virtual void addNextChainObject(std::shared_ptr<ChainObject> next);

        /**
         * Returns the list of chain objects that follow this chain object.
         *
         * @returns std::vector<std::shared_ptr<ChainObject>> The list of
         *     chain objects that follow this chain object.
         */
        virtual std::vector<std::shared_ptr<ChainObject>> getNextChainObjects();
    };
}


#endif
