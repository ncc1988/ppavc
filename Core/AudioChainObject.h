/*
    AudioChainObject.h
    This file is part of ppavc - planned program for audio and video conversion.
    Copyright (C) 2016 - 2022 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef PPAVC__AUDIOCHAINOBJECT_H
#define PPAVC__AUDIOCHAINOBJECT_H


#include <memory>
#include <vector>


#include "AudioIOFormat.h"
#include "AudioStreamConfig.h"


namespace PPAVC
{
    /**
     * An AudioChainObject implementation is a class whose instances
     * can be chained together to convert audio data.
     */
    class AudioChainObject: public std::enable_shared_from_this<AudioChainObject>
    {
        protected:


        AudioStreamConfig input_stream_config;


        AudioStreamConfig output_stream_config;


        /**
         * This is a list of pointers to the next objects in the audio chain.
         */
        std::vector<std::shared_ptr<AudioChainObject>> next_chain_objects;


        public:


        /**
         * Returns the list of audio input formats that this object
         * supports for input data (data to be encoded).
         *
         * If this list is empty, it means that the codec cannot
         * encode any data.
         */
        virtual std::vector<AudioIOFormat> getInputFormats() = 0;


        /**
         * Returns a list of audio outout formats that this object
         * supports for output (decoded) data.
         *
         * If this list is empty, it means that the codec cannot
         * decode any data.
         */
        virtual std::vector<AudioIOFormat> getOutputFormats() = 0;


        /**
         * This method is used for the last object in an audio chain.
         * That object has no next objects where it could send data,
         * so it must send them somewhere else outside of the chain.
         *
         * @param std::vector<uint8_t> data The data to send to the endpoint.
         *
         * @param bool last_chunk Whether data is the last chunk of data (true)
         *     or not (false).
         */
        virtual void sendDataToEndpoint(std::vector<uint8_t> data, bool last_chunk = false) = 0;


        /**
         * Sets the configuration for the input stream (data to be processed).
         *
         * @param const AudioStreamConfig& config The input stream configuration.
         */
        virtual void setInputStreamConfig(const AudioStreamConfig& config)
        {
            this->input_stream_config = config;
        }


        /**
         * @returns AudioStreamConfig The input stream configuration.
         */
        virtual AudioStreamConfig getInputStreamConfig()
        {
            return this->input_stream_config;
        }


        /**
         * Sets the output audio stream configuration (for processed data).
         *
         * @param const AudioStreamConfig& config The output stream configuration.
         */
        virtual void setOutputStreamConfig(const AudioStreamConfig& config)
        {
            this->output_stream_config = config;
        }


        /**
         * @returns AudioStreamConfig The output stream configuration.
         */
        virtual AudioStreamConfig getOutputStreamConfig()
        {
            return this->output_stream_config;
        }


        /**
         * Receives data, processes them and passes them on to the next
         * chain objects. If no next chain object is defined, the method
         * sendDataToEndpoint is called.
         *
         * @param std::vector<uint8_t> data The data to be received.
         *
         * @param bool last_chunk Whether data is the last chunk of data (true)
         *     or not (false).
         */
        virtual void receiveData(std::vector<uint8_t> data, bool last_chunk = false)
        {
            auto result = this->processData(data);
            if (result.empty()) {
                return;
            }
            if (this->next_chain_objects.empty()) {
                //Call the endpoint method:
                this->sendDataToEndpoint(result, last_chunk);
            } else {
                //Send the data to the next objects in the chain:
                for (auto next: this->next_chain_objects) {
                    next->receiveData(result, last_chunk);
                }
            }
        }


        /**
         * Processes data.
         *
         * @param std::vector<uint8_t> input Input data.
         *
         * @returns std::vector<uint8_t> The processed data.
         */
        virtual std::vector<uint8_t> processData(std::vector<uint8_t> input) = 0;


        virtual std::vector<std::shared_ptr<AudioChainObject>> getNextChainObjects()
        {
            return this->next_chain_objects;
        }


        static bool isInChain(
            std::shared_ptr<AudioChainObject> needle,
            std::shared_ptr<AudioChainObject> haystack
            )
        {
            if (needle == haystack) {
                return true;
            }

            if ((needle == nullptr) || (haystack == nullptr)) {
                return false;
            }

            for (auto next_haystack: haystack->getNextChainObjects()) {
                if (AudioChainObject::isInChain(needle, next_haystack)) {
                    return true;
                }
            }
            //If code execution reaches this point, the needle could not be found.
            return false;
        }


        virtual void addNextChainObject(std::shared_ptr<AudioChainObject> next)
        {
            if (AudioChainObject::isInChain(this->shared_from_this(), next)) {
                //We cannot make a circular chain!
                return;
            }
            //Now we must check if next is already in the list of
            //next chain objects:
            for (auto object: this->next_chain_objects) {
                if (object == next) {
                    //Nothing to do.
                    return;
                }
            }

            //If code execution reaches this point, we can add the next chain
            //object to this object.
            this->next_chain_objects.push_back(next);
        }
    };
}


#endif
