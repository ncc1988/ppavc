/*
    DVDResource.h
    This file is part of endemm - encoder and decoder for multimedia.
    Copyright (C) 2016 - 2024 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef ENDEMM__DVDRESOURCE_H
#define ENDEMM__DVDRESOURCE_H


#include <cinttypes>
#include <iterator>
#include <map>
#include <string>
#include <regex>


#include <dvdread/dvd_reader.h>
#include <dvdread/ifo_read.h>
#include <fmt/core.h>


#include "Resource.h"
#include "Helpers/StringHelper.h"


namespace Endemm
{
    /**
     * This enum class defines unit of measurement specific for DVDs so that
     * items and ranges thereof can be expressed in units common to DVD video.
     */
    enum class DVDUnit
    {
        /**
         * The unit of measurement is a DVD title set.
         */
        TITLE_SET,

        /**
         * The unit of measurement is a DVD title.
         */
        TITLE,

        /**
         * The unit of measurement is a DVD chapter.
         */
        CHAPTER,

        /**
         * The unit of measurement is a DVD sector/block (2048 bytes).
         * (used internally)
         */
        SECTOR,

        /**
         * The unit of measurement is a byte.
         * (unhandy, but possible)
         */
        BYTE
    };


    /**
     * DVDResource is a Resource implementation for DVD-Video media.
     */
    class DVDResource: public Resource
    {
        protected:

        /**
         * The DVD device to use. Defaults to /dev/sr0.
         */
        std::string dvd_device = "/dev/sr0";

        /**
         * The DVD reader handle from libdvdread.
         */
        dvd_reader_t* handle = nullptr;

        /**
         * The DVD file handle from libdvdread.
         */
        dvd_file_t* dvd_file_handle = nullptr;

        /**
         * The DVD item unit the DVD resource deals with.
         * Defaults to a DVD title.
         *
         * The number of the unit that the DVDResource instance shall handle,
         * is specified in the dvd_unit_number attribute.
         */
        DVDUnit resource_unit = DVDUnit::TITLE;

        /**
         * The number of the unit on the DVD the DVDResource instance shall
         * handle.
         *
         * The type of unit is defined in the resource_unit attribute.
         */
        uint64_t dvd_unit_number = 0;

        /**
         * The unit of measurement (uom) for the start and end position
         * attributes dvd_start_position and dvd_end_position.
         */
        DVDUnit position_uom = DVDUnit::SECTOR;

        /**
         * The start position on the DVD for the DVD resource.
         *
         * The unit of measurement is defined in the position_uom attribute.
         */
        uint64_t dvd_start_position = 0;

        /**
         * The end position on the DVD for the DVD resource.
         *
         * The unit of measurement is defined in the position_uom attribute.
         */
        uint64_t dvd_end_position = 0;

        /**
         * The sector on the DVD where reading should start from.
         * This is only the same as dvd_start_position if position_uom is set
         * to DVDUnit::SECTOR.
         */
        uint64_t dvd_start_sector = 0;

        /**
         * The sector on the DVD where reading should end.
         * This is only the same as dvd_end_position if position_uom is set
         * to DVDUnit::SECTOR.
         */
        uint64_t dvd_end_sector = 0;

        /**
         * The current sector of the DVD.
         */
        uint64_t dvd_current_sector = 0;

        std::vector<uint8_t> read_buffer;

        /**
         * The leftover data from the last read. This is only filled when
         * reading does not happen in 2048 byte blocks.
         */
        std::vector<uint8_t> last_read_leftover;

        public:

        /**
         * The constructor for a DVD resource.
         *
         * @param const std::string& location The DVD title number with
         * optional chapters in the following format:
         * TITLE[START_CHAPTER[-END_CHAPTER]][@DVD_DEVICE]
         *
         * The default DVD device is /dev/sr0.
         *
         * Examples:
         * - "5": Title 5
         * - "4:2: Title 4, starting from chapter 2
         * - "8:4-7: Title 8 from chapter 4 to 7
         */
        DVDResource(const std::string& location = "");

        ~DVDResource();

        /**
         * Sets the DVD device.
         *
         * @param std::string& dvd_device The DVD device.
         */
        void setDVDDevice(const std::string& dvd_device);

        /**
         * Returns the DVD device that is set.
         *
         * @returns std::string The DVD device.
         */
        std::string getDVDDevice();

        /**
         * @returns uint64_t The start sector from which reading DVD data
         *    will start. Defaults to 0.
         */
        uint64_t getStartSector();

        /**
         * @returns uint64_t The end sector on which reading DVD data
         *    will end. Defaults to 0.
         */
        uint64_t getEndSector();

        /**
         * @returns uint64_t The current sector where the next block of data
         *    will be read from the DVD if the position is not changed.
         */
        uint64_t getCurrentSector();

        /**
         * Initialises the reading of the DVD by preparing everything
         * for reading data.
         * This method is also called automatically when one of the read
         * or readObject methods is called and no initialisation has taken
         * place.
         *
         * @returns bool True on successful initialisation, false otherwise.
         */
        bool initialise();

        /**
         * @see Resorce::probe
         */
        virtual std::shared_ptr<Container> probe() override;

        /**
         * @see Resource::writeObject
         */
        virtual bool writeObject(const uint8_t* data, size_t size) override;


        /**
         * @see Resource::writeObject
         */
        virtual bool writeObject(const uint8_t* data, size_t size, size_t position) override;


        /**
         * @see Resource::readObject
         */
        virtual bool readObject(const uint8_t* data, size_t size) override;


        /**
         * @see Resource::readObject
         */
        virtual bool readObject(const uint8_t* data, size_t size, size_t position) override;


        /**
         * @see Resource::write
         */
        virtual size_t write(const std::vector<uint8_t>& data) override;


        /**
         * @see Resource::write
         */
        virtual size_t write(const std::vector<uint8_t>& data, size_t position) override;


        /**
         * @see Resource::read
         */
        virtual std::vector<uint8_t> read(size_t size) override;

        /**
         * @see Resource::read
         */
        virtual std::vector<uint8_t> read(size_t size, size_t position) override;

        /**
         * @see Resource::seekAbsolutePosition
         */
        virtual bool seekAbsolutePosition(size_t position) override;

        /**
         * @see Resource::moveForward
         */
        virtual bool moveForward(size_t offset) override;

        /**
         * @see Resource::moveBackward
         */
        virtual bool moveBackward(size_t offset) override;
    };
}


#endif
