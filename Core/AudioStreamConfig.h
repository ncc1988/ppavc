/*
    AudioStreamConfig.h
    This file is part of ppavc - planned program for audio and video conversion.
    Copyright (C) 2016 - 2022 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef PPAVC__AUDIOSTREAMCONFIG_H
#define PPAVC__AUDIOSTREAMCONFIG_H


#include <inttypes.h>


namespace PPAVC
{
    /**
     * The configuration for an audio stream.
     */
    struct AudioStreamConfig
    {
        /**
         * The amount of audio channels.
         */
        uint8_t channels = 0;


        /**
         * The amount of bits per sample.
         */
        uint8_t bits_per_sample = 0;


        /**
         * The sample rate for each channel in samples per seconds.
         */
        uint32_t sample_rate = 0;


        bool operator==(const AudioStreamConfig& other)
        {
            return this->channels == other.channels
                && this->bits_per_sample == other.bits_per_sample
                && this->sample_rate == other.sample_rate;
        }


        bool operator!()
        {
            return this->channels == 0
                || this->bits_per_sample == 0
                || this->sample_rate == 0;
        }
    };
}


#endif
