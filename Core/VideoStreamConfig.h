/*
    VideoStreamConfig.h
    This file is part of ppavc - planned program for audio and video conversion.
    Copyright (C) 2016 - 2022 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef PPAVC__VIDEOSTREAMCONFIG_H
#define PPAVC__VIDEOSTREAMCONFIG_H


namespace PPAVC
{
    /**
     * The configuration for a video stream.
     */
    struct VideoStreamConfig
    {
        /**
         * The video frame width in pixels.
         */
        uint16_t width = 0;


        /**
         * The video frame height in pixels.
         */
        uint16_t height = 0;


        /**
         * The displayed video frame width in pixels.
         * This is used for setting the aspect ratio.
         */
        uint16_t display_width = 0;


        /**
         * The displayed video frame heigth in pixels.
         * This is used for setting the aspect ratio.
         */
        uint16_t display_height = 0;
    };
}


#endif
