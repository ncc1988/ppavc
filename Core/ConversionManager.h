/*
    ConversionManager.h
    This file is part of ppavc - planned program for audio and video conversion.
    Copyright (C) 2016 - 2022 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef PPAVC__CONVERSIONMANAGER_H
#define PPAVC__CONVERSIONMANAGER_H


#include <map>
#include <memory>
#include <string>
#include <utility>


#include "Exceptions.h"
#include "FileResource.h"
#include "StreamID.h"
#include "Resource.h"
#include "../Codecs/AudioCodec.h"
#include "../Codecs/VideoCodec.h"

#include "../Codecs/PCMCodec.h" //only in early stage development!
#include "../Containers/RIFFContainer.h" //only in early stage development!
#include "../DataConverters/AudioChannelConverter.h" //only in early stage development!


namespace PPAVC
{
    /**
     * This class manages the whole conversion process.
     * It initialises the data inputs and outputs,
     * attaches processors to each other and codecs,
     * sets codec parameters etc.
     */
    class ConversionManager
    {
        protected:


        /**
         * A list of all input resources.
         */
        std::vector<std::shared_ptr<Resource>> inputs;


        /**
         * A list of all output resources.
         */
        std::vector<std::shared_ptr<Resource>> outputs;


        /**
         * An unordered list of mapping information to map input streams
         * to output streams. Input streams are the first items in the map
         * and output streams are the second items.
         */
        std::vector<std::pair<StreamID, StreamID>> mapping;


        bool use_sequential_mapping = true;


        public:


        ConversionManager();


        /**
         * By specifying two reources by their URI the ConversionManager can
         * start processing these resources.
         * In outputResourceURI is not specified the ConversionManager
         * will only read information about the input stream and exit
         * after that.
         */
        ConversionManager(std::string inputResourceURI, std::string outputResourceURI = "");


        ~ConversionManager();


        virtual std::shared_ptr<Resource> getInputResource(uint8_t resource_id = 0);


        virtual std::shared_ptr<Resource> getOutputResource(uint8_t resource_id = 0);


        virtual void addOutputResource(std::shared_ptr<Resource> resource);


        /**
         * Sets the mapping from input to output streams.
         *
         * @param std::vector<std::pair<StreamID, StreamID>> mapping The mapping
         *     from input to output streams, where the first items are input
         *     streams and the second items are output streams.
         *
         * @param bool use_sequential_mapping Whether to map streams
         *     sequentially (one after the other, true) to output resources or
         *     simultaneously (merge streams into one, false). Defaults to true.
         *
         * @throws Exception In case one of the referenced resources is invalid
         *     (non-existant), an exception is thrown.
         */
        virtual void setMapping(
            std::vector<std::pair<StreamID, StreamID>> mapping,
            bool use_sequential_mapping = true
            );


        /**
         * Checks the mapping specification and returns all incorrect mappings.
         *
         * @returns std::vector<std::pair<StreamID, std::string>> All incorrect
         *     mappings, together with a string describing the problem.
         */
        virtual std::vector<std::pair<StreamID, std::string>> checkMapping();


        /**
         * Probes the resource for finding a container that is recognised.
         *
         * @param std::shared_ptr<Resource> resource The resource to be probed.
         *
         * @returns std::shared_ptr<Container> A container for the resource
         *     or a nullptr if no supported container format could be detected.
         */
        virtual std::shared_ptr<Container> probeResource(
            std::shared_ptr<Resource> resource
            );


        /**
         * Creates all containers for all outputs.
         */
        virtual std::vector<std::shared_ptr<Container>> createOutputContainers();


        /**
         * This method starts the conversion process.
         */
        virtual void convert();
    };
}


#endif
