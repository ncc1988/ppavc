/*
    ConversionManager.h
    This file is part of endemm - encoder and decoder for multimedia.
    Copyright (C) 2016 - 2023 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef ENDEMM__CONVERSIONMANAGER_H
#define ENDEMM__CONVERSIONMANAGER_H


#include <map>
#include <memory>
#include <regex>
#include <string>
#include <utility>


#include "CodecConfig.h"
#include "Exceptions.h"

#include "StreamID.h"

//Resources:
#include "Resource.h"
#include "SimpleResourceProber.h"
#include "DVDResource.h"
#include "FileResource.h"

//Others:
#include "Helpers/StringHelper.h"
#include "../Codecs/AudioCodec.h"
#include "../Codecs/VideoCodec.h"

#include "../Codecs/PCMCodec.h" //only in early stage development!
#include "../Containers/RIFFContainer.h" //only in early stage development!
#include "../DataConverters/AudioChannelConverter.h" //only in early stage development!


namespace Endemm
{
    /**
     * This class manages the whole conversion process.
     * It initialises the data inputs and outputs,
     * attaches processors to each other and codecs,
     * sets codec parameters etc.
     */
    class ConversionManager
    {
        protected:


        /**
         * A list of all input resources.
         */
        std::map<std::string, std::shared_ptr<Resource>> inputs;


        /**
         * A list of all output resources.
         */
        std::map<std::string, std::shared_ptr<Resource>> outputs;


        /**
         * An unordered list of mapping information to map input streams
         * to output streams. Input streams are the first items in the map
         * and output streams are the second items.
         */
        std::vector<std::pair<StreamID, StreamID>> mapping;


        /**
         * The global audio codec configuration.
         */
        CodecConfig global_audio_config = {0, false, ""};


        /**
         * The global video codec configuration.
         */
        CodecConfig global_video_config = {0, false, ""};


        /**
         * The stream-specific codec configurations.
         */
        std::map<StreamID, CodecConfig> stream_codec_configs;


        bool use_sequential_mapping = true;


        bool use_json_output = false;


        bool text_output_disabled = false;


        virtual uint32_t bitrateToInt(const std::string& bitrate) noexcept;


        virtual void printJsonMessage(
            const std::string& message_type,
            const std::map<std::string, std::string>& data
            ) const;


        virtual void printConversion(
            std::map<StreamID, std::shared_ptr<Codec>> chains
            );


        /**
         * This method handles detecting resources by their URI scheme.
         */
        virtual std::shared_ptr<Resource> getResourceFromURI(std::string uri, bool write_mode = false);


        public:


        ConversionManager();


        /**
         * By specifying two reources by their URI, the ConversionManager can
         * start processing these resources.
         * If output_uri is not specified the ConversionManager
         * will only read information about the input stream and exit
         * after that.
         */
        ConversionManager(std::string input_uri, std::string output_uri = "");


        ~ConversionManager();


        virtual std::shared_ptr<Resource> getInputResource(const std::string& input_id);


        virtual std::shared_ptr<Resource> getOutputResource(const std::string& output_id);


        /**
         * Adds an input, specified by a string.
         */
        virtual void addInput(std::string input, std::string alias = "");


        /**
         * Adds an output, specified by a string.
         */
        virtual void addOutput(std::string output, std::string alias = "");


        [[deprecated]] virtual void addOutputResource(std::shared_ptr<Resource> resource);


        /**
         * Sets the output codec globally or for a specific output stream.
         *
         * @param const std::string& codec_name The name of the codec.
         *
         * @param const std::string& stream The stream for which to set the
         *     codec. If this is set to "*:A*", the codec will be used
         *     globally for all audio streams. In the same way, "*:V*" will
         *     set the codec globally for all video streams.
         */
        virtual void setCodec(const std::string& codec_name, const std::string& stream);


        /**
         * Sets the output bitrate globally or for a specific output stream.
         *
         * @param const std::string& bitrate_str The bitrate to set.
         *
         * @param const std::string& stream The stream for which to set the
         *     bitrate. If this is set to "*:A*", the codec will be used
         *     globally for all audio streams. In the same way, "*:V*" will
         *     set the codec globally for all video streams.
         */
        virtual void setBitrate(const std::string& bitrate_str, const std::string& stream);


        /**
         * Sets the output quality globally or for a specific output stream.
         *
         * @param const std::string& quality_str The quality to set.
         *
         * @param const std::string& stream The stream for which to set the
         *     quality. If this is set to "*:A*", the codec will be used
         *     globally for all audio streams. In the same way, "*:V*" will
         *     set the codec globally for all video streams.
         */
        virtual void setQuality(const std::string& quality_str, const std::string& stream);


        /**
         * Enables output of information in JSON instead of human-readable text.
         */
        virtual void enableJsonOutput();


        /**
         * Disables text output on the standard error output.
         */
        virtual void disableTextOutput();


        /**
         * Creates an audio codec instance from using a codec name and an
         * audio stream configuration.
         *
         * @param const std::string& codec_name The name of the audio codec.
         *
         * @param const AudioStreamConfig& config The stream configuration
         *     to be used.
         */
        virtual std::shared_ptr<AudioCodec> createAudioCodec(
            const std::string& codec_name,
            const AudioStreamConfig& config
            );


        /**
         * Creates a video codec instance from using a codec name and a
         * video stream configuration.
         *
         * @param const std::string& codec_name The name of the video codec.
         *
         * @param const VideoStreamConfig& config The stream configuration
         *     to be used.
         */
        virtual std::shared_ptr<VideoCodec> createVideoCodec(
            const std::string& codec_name,
            const VideoStreamConfig& config
            );


        /**
         * Sets the mapping from input to output streams.
         *
         * @param std::vector<std::pair<StreamID, StreamID>> mapping The mapping
         *     from input to output streams, where the first items are input
         *     streams and the second items are output streams.
         *
         * @param bool use_sequential_mapping Whether to map streams
         *     sequentially (one after the other, true) to output resources or
         *     simultaneously (merge streams into one, false). Defaults to true.
         *
         * @throws Exception In case one of the referenced resources is invalid
         *     (non-existant), an exception is thrown.
         */
        virtual void setMapping(
            std::vector<std::pair<StreamID, StreamID>> mapping,
            bool use_sequential_mapping = true
            );


        /**
         * Checks the mapping specification and returns all incorrect mappings.
         *
         * @returns std::vector<std::pair<StreamID, std::string>> All incorrect
         *     mappings, together with a string describing the problem.
         */
        virtual std::vector<std::pair<StreamID, std::string>> checkMapping();


        /**
         * Creates all containers for all outputs.
         */
        virtual std::vector<std::shared_ptr<Container>> createOutputContainers();


        /**
         * This method starts the conversion process.
         */
        virtual void convert();
    };
}


#endif
