/*
    FileResource.h
    This file is part of ppavc - planned program for audio and video conversion.
    Copyright (C) 2016 - 2022 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef PPAVC__FILERESOURCE_H
#define PPAVC__FILERESOURCE_H


#include <fstream>
#include <memory>


#include <fmt/core.h>


#include "Exceptions.h"
#include "Resource.h"


namespace PPAVC
{
    /**
     * FileResource is a Resource implementation for files in the file system.
     */
    class FileResource: public Resource
    {
        protected:


        /**
         * The file that is used for read/write operations.
         */
        std::unique_ptr<std::fstream> file = nullptr;


        /**
         * The size of the file in bytes.
         */
        size_t file_size = 0;


        /**
         * Opens a file.
         */
        void openFile();


        public:


        /**
         * The constructor of FileResource.
         *
         * @param const std::string& location The file name or path of the
         *     file that is being attached to this resource.
         *
         * @param bool write_mode Whether to enable write access to the file
         *     (true) or not (false). Defaults to false.
         */
        FileResource(const std::string& location, bool write_mode = false);


        /**
         * The destructor of FileResource. It is needed to close the
         * attached file.
         */
        ~FileResource();


        /**
         * @see Resource::writeObject
         */
        virtual bool writeObject(const uint8_t* data, size_t size) override;


        /**
         * @see Resource::writeObject
         */
        virtual bool writeObject(const uint8_t* data, size_t size, size_t position) override;


        /**
         * @see Resource::readObject
         */
        virtual bool readObject(const uint8_t* data, size_t size) override;


        /**
         * @see Resource::readObject
         */
        virtual bool readObject(const uint8_t* data, size_t size, size_t position) override;


        /**
         * @see Resource::write
         */
        virtual size_t write(const std::vector<uint8_t>& data) override;


        /**
         * @see Resource::write
         */
        virtual size_t write(const std::vector<uint8_t>& data, size_t position) override;


        /**
         * @see Resource::read
         */
        virtual std::vector<uint8_t> read(size_t size) override;


        /**
         * @see Resource::read
         */
        virtual std::vector<uint8_t> read(size_t size, size_t position) override;
    };
}


#endif
