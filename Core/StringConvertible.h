/*
    StringConvertible.h
    This file is part of ppavc - planned program for audio and video conversion.
    Copyright (C) 2016 - 2022 Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <string>


#ifndef PPAVC__STRINGCONVERTIBLE_H
#define PPAVC__STRINGCONVERTIBLE_H


/**
 * An interface for making objects representable as strings.
 */
class StringConvertible
{
    public:


    /**
     * @returns A string representation of the object.
     */
    virtual std::string toString() = 0;
};


#endif
