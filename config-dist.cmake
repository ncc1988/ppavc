# This is the CMake default (source code distribution) configuration.
#
# In case you want to build Endemm in a different way, you can copy
# this file to config.cmake. All changes in there will overwrite
# the configuration in here.


# endemm configuration variables:

set(ENABLE_DVD_SUPPORT 1)
